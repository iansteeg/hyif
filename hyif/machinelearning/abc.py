from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Optional, Union


class HylleraasMLInterface(ABC):
    """Base class for machinelearning."""

    @abstractmethod
    def train(self, data: Any, **kwargs):
        """Train a ML model."""
        pass

    @abstractmethod
    def test(self, dataset: Any):
        """Test a ML model."""
        pass

    @abstractmethod
    def infer(self,
              model: Optional[Any] = None,
              data: Optional[Any] = None,
              **kwargs
              ):
        """Predict data from a ML model."""
        pass

    @abstractmethod
    def check_version(self):
        """Check version."""
        pass


class Runner(ABC):
    """Runner base class."""

    @abstractmethod
    def run(self, input_dict: dict) -> Union[str, dict]:
        """Run a calculation."""
        pass


class Parser(ABC):
    """Parser base class."""

    @abstractmethod
    def parse(self, input: Union[str, Path]) -> dict:
        """Parse file or input_str."""
        pass
