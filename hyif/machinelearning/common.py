from typing import Any, List, Optional, Union

import numpy as np


class Common:
    """Common methods."""

    def get_model_deviation(self,
                            models: Union[str, List[str]],
                            reference: Any,
                            options: Optional[dict] = {}) -> np.ndarray:
        """Get model deviation."""
        if isinstance(models, str):
            models = [models]

        if not hasattr(self, 'infer'):
            raise NotImplementedError(
                'Model does not have infer method.')

        # Get model predictions
        results = []
        for model in models:
            results.append(
                self.infer(model=model,  # type: ignore
                           data=reference, options=options))

        # Prepare results for deviation calculation
        results_array = dict()
        for key in results[0].keys():
            results_array[key] = np.atleast_3d(
                np.array([result[key] for result in results]))

        # Calculate deviation
        devi = dict()
        for key in results_array.keys():

            # Check that dimensions are 3D

            if len(results_array[key].shape) == 3:
                err = np.std(results_array[key], axis=0)
            elif len(results_array[key].shape) == 4:
                err = np.var(results_array[key], axis=0).sum(axis=-1)**0.5
            else:
                raise ValueError('Dimension of results_array[key] ' +
                                 'is not 3 or 4.')

            devi[key] = np.array(
                [err.max(axis=-1),
                 err.min(axis=-1),
                 err.mean(axis=-1)]).T
        return devi
