import filecmp
import pathlib

import numpy as np
import pytest
from hyobj import Molecule, PeriodicSystem, Units
from hyset import create_compute_settings

from hyif.machinelearning.nequip import NequIP, NequIPData

cs = create_compute_settings('local', debug=True)

mynequip = NequIP({'check_version': False}, compute_settings=cs)

water = PeriodicSystem(Molecule('O'), pbc=[4, 0, 0, 0, 4, 0, 0, 0, 4])

coordinates = 5 * [water.coordinates]
forces = 5 * [water.coordinates * 0.05]
energies = 5 * [-100.0]
atoms = 5 * [water.atoms]
boxes = 5 * [water.box]
nequip_data_dict = {
    'coordinates': coordinates,
    'forces': forces,
    'energies': energies,
    'atoms': atoms,
    'boxes': boxes,
}


def test_nequipdata():
    """Test NequIPData."""
    data = NequIPData(**nequip_data_dict, frames=[0, 1])
    np.testing.assert_allclose(
        data.forces[0], np.array(forces).reshape(5, -1)[0]
    )
    assert len(data.energies) == 2
    assert data.atoms.tolist() == [['O', 'H', 'H'], ['O', 'H', 'H']]

    data = NequIPData(**nequip_data_dict)
    data.dump_data(data_dir='./data_test1', nopbc=True)
    data.dump_data(
        data_dir='./data_test1',
        data_file='test.xyz',
        overwrite=True,
        nopbc=True,
    )
    data.dump_data(
        data_dir='./data_test1',
        data_file='test.xyz',
        overwrite=True,
        nopbc=True,
    )
    with pytest.raises(FileExistsError):
        data.dump_data(
            data_dir='./data_test1', data_file='test.xyz', nopbc=True
        )

    data = NequIPData(data_file='./data_test1/test.xyz', load=True)
    data.dump_data(
        data_dir='./data_test1',
        data_file='test2.xyz',
        overwrite=True,
        nopbc=True,
    )
    assert filecmp.cmp('./data_test1/test.xyz', './data_test1/test2.xyz')

    ds = data.to_dataset()
    print(ds.data.columns)
    assert ds.data.shape == (5, 6)
    assert 'energies' in ds.data.columns

    data = NequIPData(**nequip_data_dict, units=Units('atomic'))
    np.testing.assert_allclose(
        data.energies / 27.2114, energies, atol=1e-4, rtol=1e-4
    )


def test_read_input():
    """Test reading input."""
    with pytest.raises(FileNotFoundError):
        mynequip.InputParser.parse('test')


def test_nequipdata_interface():
    """Test NequIPData interface."""
    data = NequIPData(**nequip_data_dict)
    nequip_data = mynequip.get_data(data)
    np.testing.assert_allclose(nequip_data.coordinates, data.coordinates)
    nequip_data = mynequip.get_data(nequip_data_dict)
    np.testing.assert_allclose(nequip_data.forces, data.forces)
    nequip_data = mynequip.get_data(data.to_dataset())
    np.testing.assert_allclose(nequip_data.boxes, data.boxes)
    nequip_data = mynequip.get_data(data.to_dataset().data)
    np.testing.assert_allclose(nequip_data.energies, data.energies)
    nequip_data.data_file = './data_test2/test.xyz'
    mynequip.dump_data(nequip_data, overwrite=True)
    nequip_data = mynequip.get_data('./data_test2/test.xyz', load=True)
    np.testing.assert_allclose(nequip_data.coordinates, data.coordinates)


def test_integration():
    """Test NequIP."""
    cs = create_compute_settings('local', debug=True, sub_dir='test_train')

    mynequip = NequIP({'check_version': False}, compute_settings=cs)

    v = mynequip.check_version()
    assert v is not None

    # train and deploy
    out_train = mynequip.train(
        'data_test1/test.xyz',
        program_opt={
            'seed': 123,
        },
    )
    assert pathlib.Path(out_train['model']).exists()

    # test in the config not used in the train and validation
    out_test = mynequip.test(
        out_train['train']['out_dir'],
        model=out_train['model'],
    )

    assert [
        k in ['stderr', 'stdout', 'e_rmse', 'e/N_rmse', 'f_rmse']
        for k in out_test.keys()
    ]

    assert pytest.approx(out_test['f_rmse']) == 0.119828
    assert pytest.approx(out_test['e_rmse']) == 0.002151
    assert pytest.approx(out_test['e/N_rmse']) == 0.000717

    # test infer using the same configs
    out_infer = mynequip.infer(
        data='./data_test1/test.xyz', model=out_train['model']
    )
    np.testing.assert_allclose(out_infer['energy'], energies, rtol=1e-1)
