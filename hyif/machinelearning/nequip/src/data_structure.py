import os
import re
from dataclasses import dataclass
from itertools import islice
from pathlib import Path
from typing import List, Optional, Union

import numpy as np
from hyobj import DataSet, Units

from ....manager import File, FileHandler
from .units import NequIPUnits


@dataclass
class NequIPData:
    """NequIP data class."""

    coordinates: Optional[np.ndarray] = None
    boxes: Optional[np.ndarray] = None
    energies: Optional[np.ndarray] = None
    forces: Optional[np.ndarray] = None
    virials: Optional[np.ndarray] = None
    atoms: Optional[List[str]] = None
    natoms: Optional[int] = None
    output_file: Optional[Union[str, Path, File]] = None
    model: Optional[Union[str, Path, File]] = None
    model_compiled: Optional[Union[str, Path, File]] = None
    data_file: Optional[File] = None
    dump: Optional[bool] = None
    load: Optional[bool] = None
    nopbc: Optional[bool] = None
    units: Optional[Units] = None
    frames: Optional[list] = None
    nframes: Optional[int] = None
    file_handler: Optional[FileHandler] = None

    def __post_init__(self):
        """Post init."""
        if self.load:
            self.load_data()

        self.nframes = self._set_frames()

        # if self.atoms is None:
        #     raise ValueError("atom types are not set")

        if self.units is not None:
            self._fix_input_units(self.units)
            self.units = NequIPUnits

        if self.dump:
            self.dump_data()

        self._fix_dimensions()

    def _fix_dimensions(self):
        """Fix array shapes."""
        n_data = -1
        if self.coordinates is not None:
            self.coordinates = np.array(self.coordinates)
            n_data = self.coordinates.shape[0]
            self.coordinates = self.coordinates.reshape((n_data, -1))
        if n_data == -1:
            return
        if self.atoms is not None:
            self.atoms = np.array(self.atoms).reshape((n_data, -1))
        if self.forces is not None:
            self.forces = np.array(self.forces).reshape((n_data, -1))
        if self.virials is not None:
            self.virials = np.array(self.virials).reshape((n_data, 9))
        if self.boxes is not None:
            self.boxes = np.array(self.boxes).reshape((n_data, 9))

    def _set_frames(self):
        """Set frames to be used."""
        if self.frames is not None:
            frames = list(set(self.frames))

        if self.energies is None:
            return 0

        if self.frames is None:
            frames = list(range(len(self.energies)))

        if len(frames) == 0:
            raise ValueError('frames is empty')

        self.energies = np.take(self.energies, frames, 0)

        if self.forces is not None:
            self.forces = np.take(self.forces, frames, 0)
        if self.atoms is not None:
            self.atoms = np.take(self.atoms, frames, 0)
        if self.virials is not None:
            self.virials = np.take(self.virials, frames, 0)
        if self.coordinates is not None:
            self.coordinates = np.take(self.coordinates, frames, 0)
        if self.boxes is not None:
            self.boxes = np.take(self.boxes, frames, 0)

        return len(frames)

    def _fix_input_units(self, input_units: Units):
        """Fix input units."""
        output_units = NequIPUnits

        fac_length = output_units.length[1] / input_units.length[1]
        fac_energy = output_units.energy[1] / input_units.energy[1]

        if self.coordinates is not None:
            self.coordinates *= fac_length
        if self.boxes is not None:
            self.boxes *= fac_length
        if self.energies is not None:
            self.energies *= fac_energy
        if self.forces is not None:
            self.forces *= fac_energy / fac_length
        if self.virials is not None:
            self.virials *= fac_energy

    def _parse_properties_string(self, string):
        """Parse properties string. Modified ase extxyz reader."""
        properties = {}
        properties_list = []
        dtypes = []

        fields = string.split(':')

        fmt_map = {'R': float, 'I': int, 'S': str, 'L': bool}

        for name, ptype, cols in zip(
            fields[::3], fields[1::3], [int(x) for x in fields[2::3]]
        ):
            if ptype not in ('R', 'I', 'S', 'L'):
                raise ValueError('Unknown property type: ' + ptype)

            dtype = fmt_map[ptype]
            dtypes.append(dtype)

            properties[name] = (name, cols)
            properties_list.append(name)

        slices = []
        initial_idx = 0
        for k in properties:
            slices.append(slice(initial_idx, initial_idx + properties[k][1]))
            initial_idx += properties[k][1]

        return properties, properties_list, slices, dtypes

    def _process_config(self, config_lines) -> None:
        """Process chunk of lines containing single configuration."""
        try:
            if int(config_lines[0].strip()) != self.natoms:
                raise AssertionError(
                    (
                        f'number of atoms {config_lines[0]}'
                        f' does not match {self.natoms}'
                    )
                )
        except ValueError:
            raise ValueError(
                f'number of atoms {config_lines[0]} is not an integer'
            )

        equal_regex = r'\s*=\s*("[^"]*"|\S+)'
        energy = re.findall('energy' + equal_regex, config_lines[1])[0]
        try:
            energy = float(energy)
        except ValueError:
            raise ValueError(f'energy {energy} is not a float')

        try:
            lattice = re.findall('Lattice' + equal_regex, config_lines[1])[
                0
            ].strip('"')
        except IndexError:
            lattice = ''
        try:
            if len(lattice.split()) == 9:
                lattice = np.array(lattice.split(), dtype=float)
            elif len(lattice.split()) == 0:
                lattice = np.zeros(9, dtype=float)
            else:
                raise AssertionError(f'lattice {lattice} has wrong shape')
        except ValueError:
            raise ValueError(f'lattice {lattice} is not a float array')

        try:
            virial = re.findall('virial' + equal_regex, config_lines[1])[
                0
            ].strip('"')
        except IndexError:
            virial = ''
        try:
            if len(virial.split()) == 9:
                virial = np.array(virial.split(), dtype=float)
            elif len(virial.split()) == 0:
                virial = np.zeros(9, dtype=float)
            else:
                raise AssertionError(f'virial {virial} has wrong shape')
        except ValueError:
            raise ValueError(f'virial {virial} is not a float array')

        properties = re.findall('Properties' + equal_regex, config_lines[1])[0]
        (
            properties,
            properties_list,
            slices,
            dtype,
        ) = self._parse_properties_string(properties)

        types = []
        positions = []
        forces = []

        # now for each line populate the arrays
        for line in config_lines[2:]:
            split_line = line.split()

            index = properties_list.index('species')
            try:
                types.append(
                    np.array(split_line[slices[index]], dtype=dtype[index])
                )
            except ValueError:
                raise ValueError(
                    (
                        'could not read atom type - '
                        f'something is wrong with line: {line}'
                    )
                )

            index = properties_list.index('pos')
            try:
                positions.append(
                    np.array(split_line[slices[index]], dtype=dtype[index])
                )
            except ValueError:
                raise ValueError(
                    (
                        'could not read coordinates - '
                        f'something is wrong with line: {line}'
                    )
                )

            index = properties_list.index('forces')
            try:
                forces.append(
                    np.array(split_line[slices[index]], dtype=dtype[index])
                )
            except ValueError:
                raise ValueError(
                    (
                        'could not read forces - '
                        f'something is wrong with line: {line}'
                    )
                )

        # store info for the configuration and return
        self.energies = np.append(self.energies, energy)
        self.boxes = np.vstack((self.boxes, lattice))
        self.virials = np.vstack((self.virials, virial))
        self.atoms = np.vstack((self.atoms, np.array(types).flatten()))
        self.coordinates = np.vstack(
            (self.coordinates, np.array(positions).flatten())
        )
        self.forces = np.vstack((self.forces, np.array(forces).flatten()))

    def load_data(self, **kwargs):
        """Load NequIP data in .extxyz format."""
        if self.coordinates is not None:
            return

        if self.data_file is None:
            raise ValueError('data_file is not set')
        if isinstance(self.data_file, File):
            data_file = self.data_file.path
        elif isinstance(self.data_file, (str, Path)):
            data_file = Path(self.data_file)
            if not data_file.is_file():
                raise FileNotFoundError(f'file {data_file} not found')
            self.data_file = File(name=data_file, handler=self.file_handler)
        else:
            raise ValueError('data_file must be a string, Path or File')

        if not data_file.is_file():
            raise FileNotFoundError(f'file {data_file} not found')

        # process trajectory chunk by chunk
        # is it worth to add an extra dependency?
        # https://github.com/libAtoms/extxyz
        with open(data_file, 'r') as f:
            try:
                self.natoms = int(f.readline().strip())
            except ValueError:
                raise ValueError(f'{data_file} is an invalid .xyz file')

            # initialize arrays
            self.energies = np.empty((0))
            self.boxes = np.empty((0, 9))
            self.virials = np.empty((0, 9))
            self.atoms = np.empty((0, self.natoms))
            self.coordinates = np.empty((0, 3 * self.natoms))
            self.forces = np.empty((0, 3 * self.natoms))

            f.seek(0)
            chunk_size = self.natoms + 2

            for config_chunk in iter(lambda: tuple(islice(f, chunk_size)), ()):
                if int(config_chunk[0].strip()) != self.natoms:
                    raise AssertionError(
                        (
                            f'all configurations in {self.data_file}'
                            ' must have the same number of atoms'
                        )
                    )
                elif len(config_chunk) != chunk_size:
                    raise AssertionError(
                        f'problem reading configuration in {self.data_file}'
                    )
                self._process_config(config_chunk)

    def dump_data(self, **kwargs):
        """Dump NequIP data."""
        path = kwargs.get('data_dir', '.')
        file_name = kwargs.get('data_file', self.data_file)
        overwrite = kwargs.get('overwrite', False)
        units = kwargs.get('units', None)
        nopbc = kwargs.get('nopbc', False)

        if path is None:
            path = Path('.')
        elif not isinstance(path, Path):
            path = Path(path)
        if not path.is_dir():
            path.mkdir(parents=True)

        if file_name is None:
            file_name = 'set_000.xyz'
            set_number = 0
            while Path(os.path.join(path, file_name)).is_file():
                set_number += 1
                file_name = f'set_{set_number:03d}.xyz'
            file_name = path / file_name
        elif isinstance(file_name, File):
            file_name = file_name.path
        elif isinstance(file_name, (str, Path)):
            file_name = Path(file_name)
            file_name = path / file_name
        else:
            raise ValueError('file_name must be a string, Path or File')
        if file_name.is_file() and not overwrite:
            raise FileExistsError(
                f'file {file_name} already exists and overwrite is False'
            )
        if not file_name.parent.exists():
            file_name.parent.mkdir(parents=True)

        if units is not None:
            fac_e = units.energy[1] / NequIPUnits.energy[1]
            fac_l = units.length[1] / NequIPUnits.length[1]
        else:
            fac_e = 1.0
            fac_l = 1.0

        dump_list = [
            self.atoms,
            self.energies * fac_e,
            self.coordinates * fac_l,
            self.boxes * fac_l,
            self.forces * fac_e / fac_l,
        ]
        if self.virials is not None and self.virials.all() != 0.0:
            dump_list.append(self.virials * fac_e)
        else:
            dump_list.append([None] * len(self.energies))

        # generate .extxyz string
        extxyz_str = ''
        for at, e, c, b, f, v in zip(*dump_list):
            extxyz_str += f'{len(at)}\n'
            extxyz_str += f'Lattice="{b[0]} {b[1]} {b[2]} {b[3]} {b[4]}'
            extxyz_str += f' {b[5]} {b[6]} {b[7]} {b[8]}"'
            if v is not None:
                extxyz_str += f' virial="{v[0]} {v[1]} {v[2]} {v[3]} {v[4]}'
                extxyz_str += f' {v[5]} {v[6]} {v[7]} {v[8]}"'
            if nopbc:
                extxyz_str += ' pbc="F F F"'
            else:
                extxyz_str += ' pbc="T T T"'
            extxyz_str += ' Properties=species:S:1:pos:R:3:forces:R:3'
            extxyz_str += f' energy={e}\n'
            c = c.reshape(-1, 3)
            f = f.reshape(-1, 3)
            for a, c, f in zip(at, c, f):
                extxyz_str += (
                    f'{a} {c[0]} {c[1]} {c[2]} {f[0]} {f[1]} {f[2]}\n'
                )

        with open(file_name, 'w') as f:
            f.write(extxyz_str)

    def to_dataset(self, keys: Optional[List[str]] = None):
        """Convert NequIP data to DataSet."""
        if keys is None:
            keys = [
                'energies',
                'forces',
                'virials',
                'coordinates',
                'boxes',
                'atoms',
            ]
        d = {
            k: np.array(getattr(self, k))
            for k in keys
            if getattr(self, k) is not None
        }
        return DataSet(d)
