from pathlib import Path
from typing import Union

try:
    import yaml
except ImportError:
    yaml = None


class InputParser:
    """NequIP input parser."""

    def parse(self, input: Union[str, Path]) -> dict:
        """Read NequIP input."""
        with open(input, 'r') as f:
            return yaml.safe_load(f)
