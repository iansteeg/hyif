import copy
import warnings
from dataclasses import replace
from functools import singledispatchmethod
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

import numpy as np
import packaging.version as pv

try:
    import yaml
except ImportError:
    yaml = None

try:
    import pandas as pd

    df_type = pd.DataFrame
except ImportError:
    pd = None
    df_type = Any

from hyobj import DataSet
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings)

from ....manager import File, acompute, compute
from ....utils import is_file, unique_filename
from ...abc import HylleraasMLInterface, Parser
from ...common import Common
from ...deepmd import DeepMDData
from .data_structure import NequIPData
from .input_parser import InputParser
from .units import NequIPUnits

VERSIONS_SUPPORTED = ['0.5.6']

data_type = Union[NequIPData, DataSet, str, Path]

DEFAULT_INPUT = {
    'root': '.',
    'run_name': 'default',
    'seed': 123,
    'dataset_seed': 456,
    'num_basis': 8,
    'r_max': 4.0,
    'l_max': 1,
    'parity': True,
    'num_features': 16,
    'dataset': 'ase',
    'ase_args': {'format': 'extxyz'},
    'dataset_file_name': '{{dataset_file}}',
    'wandb': False,
    'n_train': 2,
    'n_val': 1,
    'batch_size': 1,
    'validation_batch_size': 1,
    'max_epochs': 1,
    'loss_coeffs': ['forces', 'total_energy'],
    'optimizer_name': 'Adam',
    'append': True,
    'metrics_components': [
        ['total_energy', 'rmse'],
        ['total_energy', 'rmse', {'PerAtom': True}],
        ['forces', 'rmse'],
        ['forces', 'rmse'],
    ],
}


class NequIP(HylleraasMLInterface, Common):
    """NequIP interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        """Initialize.

        Initializes the class instance with the given method and
        compute_settings.

        Args:
        ----
            method (dict): A dictionary containing the method information.
            compute_settings (Optional[ComputeSettings]): An optional
                ComputeSettings object.

        Return:
        ------
            None

        """
        if yaml is None:
            raise ImportError('yaml not installed')
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.method = method
        self.Runner = self.compute_settings.Runner
        self.InputParser: Parser = InputParser()  # type: ignore
        self.arch_type = getattr(self.compute_settings, 'arch_type', 'remote')

        self.input: str = method.get('input', DEFAULT_INPUT)

        # TODO: implement restart
        files_for_restarting: List[Path] = []

        defaults = {
            'program': '',  # adjusted in each setup as argument
            'files_for_restarting': files_for_restarting,
        }
        self.run_settings = replace(
            self.compute_settings.run_settings, **defaults
        )

        self.version: pv.Version = (
            self.check_version()
            if self.method.get('check_version',
                               self.compute_settings.arch_type == 'local')
            else None
        )

        self.debug = self.method.get('debug', False)

    @singledispatchmethod
    def get_data(self, data, **kwargs) -> NequIPData:
        """Get data from input."""
        raise TypeError('data must be a NequIPData, a DataSet, dict or a path')

    @get_data.register(Path)
    @get_data.register(str)
    def _(self, data: Union[str, Path], **kwargs):
        """Get data from input."""
        run_settings = kwargs.get('run_settings', self.run_settings)

        if Path(data).is_dir():
            # this is a DeepMD input, convert to NequIP
            input_string = DeepMDData(data_dir=data, **kwargs).to_nequip()
            input_filename = unique_filename(input_string.split('\n')) + '.xyz'
            path = File(
                name=Path(data) / input_filename,
                content=input_string,
                handler=run_settings.file_handler,
            )

            write_file = path.work_path_local  # this is always done locally

            if not write_file.parent.exists():
                write_file.parent.mkdir(parents=True)

            with open(write_file, 'w') as f:
                f.write(input_string)

            return NequIPData(
                data_file=write_file,
                file_handler=run_settings.file_handler,
                **kwargs,
            )

        elif Path(data).is_file():
            data_file = File(name=data, handler=run_settings.file_handler)
            return NequIPData(data_file=data_file, **kwargs)
        else:
            raise ValueError(f'data {data} must be a directory or a file')

    @get_data.register(File)
    def _(self, data: File, **kwargs):
        """Get data from input."""
        return NequIPData(data_file=data, **kwargs)

    @get_data.register(NequIPData)
    def _(self, data: NequIPData, **kwargs):
        """Get data from input."""
        return data

    @get_data.register(DataSet)
    @get_data.register(df_type)
    @get_data.register(dict)
    def _(self, data: Union[DataSet, dict], **kwargs):
        if isinstance(data, DataSet):
            data = data.data.to_dict(orient='list')
        elif isinstance(data, df_type):
            data = data.to_dict(orient='list')
        nequip_data_dict = {}

        for k, v in data.items():
            if k in ['coord', 'coordinates']:
                nequip_data_dict['coordinates'] = np.array(v)
            if k in ['energy', 'energies']:
                nequip_data_dict['energies'] = np.array(v)
            if k in ['force', 'forces', 'gradient', 'gradients', 'grad']:
                forces = np.array(v)
                if k.startswith('grad'):
                    forces = -forces
                nequip_data_dict['forces'] = forces
            if k in ['box', 'boxes', 'unit_cell', 'pbc']:
                nequip_data_dict['boxes'] = np.array(v)
            if k in ['virial', 'virials']:
                nequip_data_dict['virials'] = np.array(v)
            if k in ['type_raw']:
                if 'type_map_raw' in nequip_data_dict:
                    nequip_data_dict['atoms'] = np.array(
                        [nequip_data_dict['type_map_raw'][int(i)] for i in v],
                        dtype=str,
                    )
                    del nequip_data_dict['type_map_raw']
                else:
                    nequip_data_dict['type_raw'] = v
            if k in ['type_map_raw']:
                if 'type_raw' in nequip_data_dict:
                    nequip_data_dict['atoms'] = np.array(
                        [v[int(i)] for i in nequip_data_dict['type_raw']],
                        dtype=str,
                    )
                    del nequip_data_dict['type_raw']
                else:
                    nequip_data_dict['type_map_raw'] = v
            if k in ['atoms']:
                nequip_data_dict['atoms'] = v

        return NequIPData(**nequip_data_dict, **kwargs)

    @singledispatchmethod
    def dump_data(self, data, **kwargs) -> None:
        """Dump data to file."""
        raise TypeError('data must be a NequIPData')

    @dump_data.register(NequIPData)
    def _(self, data: NequIPData, **kwargs):
        """Dump data to file."""
        data.dump_data(**kwargs)

    def check_version(self):
        """Check deepmd version."""
        version = None
        try:
            import nequip

            version = pv.parse(nequip.__version__)
        except ImportError:
            raise ImportError('nequip not installed.')

        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' NequIP version {version} not supported.',
                f' Please contact {self.author} ',
            )
        return version

    def author(self):
        """Get author of this interface."""
        return 'h.m.cezar@kjemi.uio.no'

    async def atrain(self, data: data_type, **kwargs) -> dict:
        """Train model asynchronously.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        setup_train = self.setup(data, train=True, run_opt=run_opt, **kwargs)
        transfer = NequIPData(output_file=setup_train.output_file.name)
        setup_deploy = self.setup(transfer, deploy=True, run_opt=run_opt)
        setup_deploy.data_files = []
        transfer.model = setup_deploy.output_file.name

        output_unparsed = await self.compute_settings.arun(
            [setup_train, setup_deploy]
        )

        output = dict()
        train = self.parse(output_unparsed[0])
        deploy = self.parse(output_unparsed[1])
        output.update({'train': train})
        output.update({'deploy': deploy, 'model': deploy['model']})

        if self.debug:
            print(f'TRAINING RESULTS:\n {output}')

        return output

    def train(self, data, **kwargs):
        """Train model.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        setup_train = self.setup(data, train=True, run_opt=run_opt, **kwargs)
        transfer = NequIPData(output_file=setup_train.output_file.name)
        setup_deploy = self.setup(transfer, deploy=True, run_opt=run_opt)
        setup_deploy.data_files = []
        transfer.model = setup_deploy.output_file.name

        output_unparsed = self.compute_settings.run(
            [setup_train, setup_deploy]
        )

        output = dict()
        train = self.parse(output_unparsed[0])
        deploy = self.parse(output_unparsed[1])
        output.update({'train': train})
        output.update({'deploy': deploy, 'model': deploy['model']})

        if self.debug:
            print(f'TRAINING RESULTS:\n {output}')

        return output

    def setup(
        self, data: Union[list, DataSet, NequIPData, str, Path, None], **kwargs
    ) -> RunSettings:
        """Set up the run."""
        run_settings = copy.deepcopy(self.run_settings)

        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        program_opt = kwargs.pop('program_opt', None)
        if program_opt is None:
            program_opt = {}

        if data is None:
            return run_settings

        nopt = sum(
            map(
                bool,
                [
                    kwargs.get('train'),  # nequip-train
                    kwargs.get('deploy'),  # nequip-deploy
                    kwargs.get('test'),  # nequip-evaluate
                    kwargs.get('infer'),  # ase
                    kwargs.get('infer_external'),  # ase
                ],
            )
        )
        if nopt != 1:
            raise ValueError(
                'only one of train, deploy, test or infer has to be True'
            )

        for k in ['train', 'deploy', 'test', 'infer', 'infer_external']:
            if kwargs.pop(k, False):
                return getattr(self, f'setup_{k}')(
                    run_settings, data, **program_opt, **kwargs
                )

        raise ValueError('No valid option for setup')

    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        result: Dict[str, Union[str, list, dict, float, np.ndarray]] = {}

        units = kwargs.get('units', None)

        if units is not None:
            e_fac = units.energy[1] / NequIPUnits.energy[1]
            f_fac = e_fac / (units.length[1] / NequIPUnits.length[1])
        else:
            e_fac, f_fac = (1.0, 1.0)

        if self.debug:
            print('DEBUG: PARSER, output:', output)
        if output.files_to_parse:
            if not isinstance(output.files_to_parse, list):
                output.files_to_parse = [output.files_to_parse]
            for f in output.files_to_parse:
                if isinstance(f, File):
                    p = Path(f.work_path_local)
                elif isinstance(f, (str, Path)):
                    p = Path(f)
                else:
                    raise TypeError(
                        'files_to_parse must be a list of File, str or Path'
                    )
                if p.name == 'log':
                    with open(p, 'rt') as ff:
                        result[str(p)] = ff.read()
                if p.suffix == '.yaml':
                    with open(p, 'rt') as ff:
                        result.update(yaml.load(ff, Loader=yaml.Loader))
                if p.name == 'infer_energies.npy':
                    result['energy'] = np.load(p) * e_fac
                if p.name == 'infer_forces.npy':
                    result['force'] = np.load(p) * f_fac
                if p.name == 'infer_virials.npy' and p.is_file():
                    result['virial'] = np.load(p) * e_fac
                if p.name == 'best_model.pth':
                    result['out_dir'] = p.parent
                    result['model'] = str(p)
                if p.suffix == '.csv':
                    try:
                        df = pd.read_csv(p)
                    except Exception:
                        raise Exception('could not read output file')

                    fields = df.columns.values.tolist()
                    if 'training_e_rmse' in fields:
                        result['energy_rmse'] = df['training_e_rmse'].values
                    if 'training_e/N_rmse' in fields:
                        result['energy_rmse_per_atom'] = df[
                            'training_e/N_rmse'
                        ].values
                    if 'training_f_rmse' in fields:
                        result['force_rmse'] = df['training_f_rmse'].values
                    if 'training_f/N_rmse' in fields:
                        result['force_rmse_per_atom'] = df[
                            'training_f/N_rmse'
                        ].values
                    if 'training_virial_rmse' in fields:
                        result['virial_rmse'] = df[
                            'training_virial_rmse'
                        ].values
                    if 'training_virial_rmse_per_atom' in fields:
                        result['virial_rmse_per_atom'] = df[
                            'training_virial_rmse_per_atom'
                        ].values

        if output.output_file:
            try:
                output_file = Path(output.output_file)
            except (ValueError, OSError):
                raise ValueError('output_file not found')

            if output_file.suffix == '.pth':
                result['model'] = str(output_file)

            if 'output_train.out' in str(output_file):
                with open(output_file, 'r') as f:
                    for line in f:
                        if line.strip().startswith('e_rmse'):
                            result['e_rmse'] = float(line.split('=')[1])
                        if line.strip().startswith('e/N_rmse'):
                            result['e/N_rmse'] = float(line.split('=')[1])
                        if line.strip().startswith('f_rmse'):
                            result['f_rmse'] = float(line.split('=')[1])
            result['output_file'] = str(output_file)

        if output.stdout:
            result['stdout'] = output.stdout

        if output.stderr:
            result['stderr'] = output.stderr

        return result

    def setup_deploy(
        self, run_settings: RunSettings, data, **kwargs
    ) -> RunSettings:
        """Set up the deploy of a model."""
        running_dict = {}

        output_file = data.output_file
        if output_file is None:
            raise ValueError('output_file was not specified')
        filename = kwargs.pop('out_file', f'{Path(output_file).name}' + '.pth')

        input_dir = File(name=output_file, handler=run_settings.file_handler)
        out_file = File(name=filename, handler=run_settings.file_handler)

        args = [
            'build',
            '--train-dir',
            str(input_dir.path.name),
            str(out_file.path),
        ]
        output_file = out_file

        if self.debug:
            print('DEBUG: DEPLOYING MODEL FROM:', str(input_dir.path))

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = '.'

        running_dict.update(
            {
                'program': 'nequip-deploy',
                'args': args,
                'output_file': output_file,
                'sub_dir': sub_dir,
                'data_files': [input_dir],
            }
        )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('DEBUG: RUN SETTINGS DEPLOY:', run_settings)

        return run_settings

    def setup_train(
        self,
        run_settings: RunSettings,
        data: Union[list, DataSet, NequIPData, str, Path, File],
        **kwargs,
    ) -> RunSettings:
        """Set up the training of a model."""
        running_dict: Dict[str, Any] = {}

        if not isinstance(data, list):
            data = [data]

        train_args = kwargs.pop('train_args', [])
        load = kwargs.pop('load', True)
        seed = kwargs.pop('seed', 1)
        log_batch_freq = kwargs.pop('log_batch_freq', 1)
        log_epoch_freq = kwargs.pop('log_epoch_freq', 1)
        max_epochs = kwargs.pop('max_epochs', 10)
        root_dir = kwargs.pop('root', '.')
        run_name = kwargs.pop('run_name', 'output')

        nequip_data = []
        for d in data:
            nequip_data.append(self.get_data(d, load=load, **kwargs))

            if nequip_data[-1].data_file is None:
                warnings.warn('NequIP data was not found on disk!')

        paths: List[File] = []
        for d in nequip_data:
            if d.data_file is None:
                continue
            if isinstance(d.data_file, (str, Path)):
                with open(d.data_file, 'r') as f:
                    paths.append(
                        File(
                            name=str(d.data_file),
                            content=f.read(),
                            handler=run_settings.file_handler,
                        )
                    )
            elif isinstance(d.data_file, File):
                paths.append(d.data_file)
            else:
                raise ValueError('data_file is not a string, Path or File')

        if len(paths) == 0:
            raise AssertionError('no data to train!')

        input_dict = kwargs.get('input', self.input)

        if len(paths) > 1:
            input_str_vars = {'dataset_file': self._concatenate_inputs(paths)}
        else:
            if self.arch_type != 'local':
                datafile_path = paths[0].data_path_remote
            else:
                datafile_path = paths[0].data_path_local

            input_str_vars = {'dataset_file': datafile_path}

        input_dict['seed'] = seed
        input_dict['dataset_seed'] = seed
        input_dict['root'] = root_dir

        unique_indexes = np.unique(nequip_data[0].atoms[0], return_index=True)[
            1
        ]
        input_dict['chemical_symbols'] = [
            str(nequip_data[0].atoms[0][i]) for i in sorted(unique_indexes)
        ]
        input_dict['log_batch_freq'] = log_batch_freq
        input_dict['log_epoch_freq'] = log_epoch_freq
        input_dict['max_epochs'] = max_epochs
        input_dict['run_name'] = run_name

        # folder_name
        input_str = yaml.dump(input_dict, indent=4, sort_keys=False)

        folder_name = unique_filename(input_str.split('\n'))

        input_file = File(
            name=folder_name + '.yaml',
            content=input_str,
            variables=input_str_vars,
            handler=run_settings.file_handler,
        )

        files_to_write = [input_file]

        args = [input_file]

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = Path('.')

        files_to_parse = [
            File(
                name=sub_dir / root_dir / run_name / 'metrics_epoch.csv',
                handler=run_settings.file_handler,
            ),
            File(
                name=sub_dir / root_dir / run_name / 'best_model.pth',
                handler=run_settings.file_handler,
            ),
            File(
                name=sub_dir / root_dir / run_name / 'config.yaml',
                handler=run_settings.file_handler,
            ),
        ]

        output_file = Path(root_dir) / run_name

        running_dict.update(
            {
                'program': 'nequip-train',
                'files_to_write': files_to_write,
                'args': args + train_args,
                'files_to_parse': files_to_parse,
                'data_files': paths,
                'output_file': output_file,
                'sub_dir': sub_dir,
            }
        )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('DEBUG: RUN SETTINGS TRAIN:', run_settings)
        return run_settings

    def setup_test(self, run_settings, data, **kwargs) -> RunSettings:
        """Set up the testing of a model."""
        running_dict = {}

        try:
            if isinstance(data, NequIPData):
                data_path = Path(data.output_file).parent
            else:
                data_path = Path(data)
        except (ValueError, OSError):
            raise ValueError('data not found')

        if not data_path.is_dir():
            raise AssertionError(
                'the data for testing must be the output directory of training'
            )

        data_dir = File(name=data_path, handler=run_settings.file_handler)

        model = kwargs.get('model', None)
        if model is None and isinstance(data, NequIPData):
            model = data.model_compiled
        if model is None:
            raise ValueError('model not specified')

        model_file = File(name=model, handler=run_settings.file_handler)

        args = [
            '--model',
            str(model_file.path.name),
            '--train-dir',
            str(data_dir.path.name),
        ]
        output_file = File(
            name='output_train.out', handler=run_settings.file_handler
        )

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = Path('.')

        running_dict.update(
            {
                'program': 'nequip-evaluate',
                'args': args,
                'output_file': output_file,
                'stdout_redirect': output_file,
                'data_files': [data_dir, model_file],
                'sub_dir': sub_dir,
            }
        )

        if self.debug:
            print('DEBUG: RUN SETTINGS TEST:', run_settings)

        return replace(run_settings, **running_dict)

    def preload_model(self, model: str, **kwargs) -> Any:
        """Preload model."""
        if not is_file(model):
            raise FileNotFoundError(f'could not find model {model}')

        species_to_type_name = kwargs.get('species_to_type_name', None)
        if species_to_type_name is not None:
            if not isinstance(species_to_type_name, dict):
                raise ValueError('species_to_type_name must be a dictionary')
        try:
            from nequip.ase import NequIPCalculator
        except ImportError:
            raise ImportError('could not find nequip')

        return NequIPCalculator.from_deployed_model(
            model_path=model, species_to_type_name=species_to_type_name
        )

    def setup_infer_external(
        self,
        run_settings: RunSettings = None,
        data: Union[str, Path] = None,
        **kwargs,
    ) -> dict:
        """Infer result from model."""
        model = kwargs.pop('model', None)

        if model is None:
            raise ValueError('model not specified')
        if not isinstance(model, (str, Path, File)):
            raise ValueError('model must be a string, Path or File')

        nequip_data = self.get_data(data, load=True)

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = Path('.')
        else:
            sub_dir = Path(sub_dir)

        species_to_type_name = kwargs.get('species_to_type_name', None)
        input_str_vars = {'species_to_type_name': species_to_type_name}

        data_files = [nequip_data.data_file]
        input_str_vars['traj_name'] = str(nequip_data.data_file.path.name)

        if isinstance(model, (str, Path)):
            input_str_vars['model'] = Path(model).name
        elif isinstance(model, File):
            input_str_vars['model'] = model.data_path_remote
        data_files.append(
            File(name=str(model), handler=run_settings.file_handler)
        )

        infer_energy = File(
            name='infer_energies.npy', handler=run_settings.file_handler
        )
        infer_force = File(
            name='infer_forces.npy', handler=run_settings.file_handler
        )
        infer_virial = File(
            name='infer_virials.npy', handler=run_settings.file_handler
        )
        input_str_vars['out_energies'] = str(infer_energy.path)
        input_str_vars['out_forces'] = str(infer_force.path)
        input_str_vars['out_virials'] = str(infer_virial.path)

        files_parse = [infer_energy, infer_force, infer_virial]

        input_str = 'import numpy as np\n'
        input_str += 'from ase.io import iread\n'
        input_str += 'from ase.calculators.calculator import '
        input_str += 'PropertyNotImplementedError\n'
        input_str += 'from nequip.ase import NequIPCalculator\n'
        input_str += 'calculator = NequIPCalculator.from_deployed_model('
        input_str += 'model_path="{{model}}",'
        input_str += 'species_to_type_name={{species_to_type_name}})\n'
        input_str += 'energies = []\n'
        input_str += 'forces = []\n'
        input_str += 'virials = []\n'
        input_str += (
            'for config in iread("{{traj_name}}", index=":", parallel=True):\n'
        )
        input_str += '    config.calc = calculator\n'
        input_str += '    energies.append(config.get_potential_energy())\n'
        input_str += '    forces.append(config.get_forces())\n'
        input_str += '    try:\n'
        input_str += '        virial = config.get_stress(voigt=False)\n'
        input_str += '        virials.append(virial)\n'
        input_str += '    except PropertyNotImplementedError:\n'
        input_str += '        pass  # model has no Virial\n'
        input_str += 'np.save("{{out_energies}}", energies)\n'
        input_str += 'np.save("{{out_forces}}", forces)\n'
        input_str += 'if len(virials) > 0:\n'
        input_str += '    np.save("{{out_virials}}", virials)\n'

        input_filename = unique_filename(input_str.split('\n')) + '.py'
        input_file = File(
            name=input_filename, content=input_str, variables=input_str_vars
        )
        args = [str(input_file.name)]
        files_to_write = [input_file]
        output_file = File(name='infer.out', handler=run_settings.file_handler)

        running_dict = {
            'program': 'python',
            'args': args,
            'files_to_write': files_to_write,
            'files_to_parse': files_parse,
            'data_files': data_files,
            'output_file': output_file,
            'sub_dir': str(sub_dir),
        }
        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('DEBUG: RUN SETTINGS INFER EXTERNAL:', run_settings)

        return run_settings

    def infer(
        self,
        model: Optional[data_type] = None,
        data: Optional[data_type] = None,
        **kwargs,
    ):
        """Infer result from model."""
        # should we have an option to select either ase or
        # nequip-evaluate with --dataset-config and --output?
        if kwargs.get('run_external', False):
            result = self.run(data, infer_external=True, model=model, **kwargs)
            return result

        if model is None:
            raise ValueError('model not specified')
        if not isinstance(model, (str, Path, File)):
            raise ValueError('model must be a string, Path or File')
        run_settings = kwargs.pop('run_settings', None)
        if run_settings is None:
            run_settings = copy.deepcopy(self.run_settings)
        if isinstance(model, (str, Path)):
            calculator = self.preload_model(str(model))
        else:
            raise AssertionError('model must be a string or a path')

        try:
            from ase.calculators.calculator import PropertyNotImplementedError
            from ase.io import iread
        except ImportError:
            raise ImportError('could not find ase')

        nequip_data = self.get_data(data, load=True)

        units = kwargs.get('units', None)

        if units is not None:
            e_fac = units.energy[1] / NequIPUnits.energy[1]
            f_fac = e_fac / (units.length[1] / NequIPUnits.length[1])
        else:
            e_fac, f_fac = (1.0, 1.0)

        energies = []
        forces = []
        virials = []
        for config in iread(
            str(nequip_data.data_file.path), index=':', parallel=True
        ):
            config.calc = calculator
            energies.append(config.get_potential_energy())
            forces.append(config.get_forces())
            try:
                virial = config.get_stress(voigt=False)
                virials.append(virial)
            except PropertyNotImplementedError:
                pass  # model has no virial

        result = {
            'energy': np.array(energies) * e_fac,
            'force': np.array(forces) * f_fac,
        }
        if len(virials) > 0:
            result['virial'] = np.array(virials) * e_fac

        return result

    def test(self, data, **kwargs):
        """Test model."""
        return self.run(data, test=True, **kwargs)

    def _concatenate_inputs(self, paths: List[File]) -> str:
        """Concatenate paths in a single input."""
        raise NotImplementedError('Not implemented yet!')

    def run(self, data, *args, **kwargs):
        """Run calculation context-based."""
        with compute(self, data, *args, **kwargs) as r:
            return r

    async def arun(self, data, *args, **kwargs):
        """Run calculation context-based and asyncronous."""
        async with acompute(self, data, *args, **kwargs) as r:
            return r
