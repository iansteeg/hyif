import warnings
from copy import deepcopy
from dataclasses import replace
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
from hyobj import Constants, MoleculeLike, PeriodicSystem, TextLikeInput, Units
from hyset import (ComputeResult, ComputeSettings, File, RunSettings,
                   create_compute_settings, run_sequentially)
from qcelemental import periodictable as pt

from ....manager import NewRun
from ....utils import unique_filename
from ...abc import HylleraasMDInterface, Parser, Runner
from .output_parser import OutputParser

VERSIONS_SUPPORTED = ['LAMMPS (29 Sep 2021 - Update 3)']

DEFAULT_INPUT = """variable	T equal {{temperature}}
variable	P equal {{pressure}}
variable	dt equal {{timestep}}
variable	nsteps equal {{nsteps}}
variable	dump_freq equal {{dump_freq}}
variable	thermo_freq equal ${dump_freq}
units	metal
boundary	p p p
atom_style	atomic
neighbor	2.0 bin
neigh_modify	every 25 delay 0 check no
box tilt large
read_data	{{hash}}.data
pair_style	lj/cut 2.5
pair_coeff	* * 1 1
velocity	all create ${T} 19949 rot yes dist gaussian
velocity	all zero linear
velocity	all zero angular
fix	1 all npt temp ${T} ${T} 0.5 iso ${P} ${P} 1.0

{{fixes}}
timestep	${dt}
"""
DEFAULT_INPUT += 'thermo_style	custom step etotal pe ke enthalpy density '
DEFAULT_INPUT += 'lx ly lz vol pxx pyy pzz press\n'
DEFAULT_INPUT += """thermo	${thermo_freq}
thermo_modify	flush yes
"""
DEFAULT_INPUT += (
    'fix	thermo_print all print ${dump_freq} "$(step) $(time) $(temp) '
)
DEFAULT_INPUT += '$(etotal) $(pe) $(ke) $(enthalpy) $(density) $(lx) $(ly) '
DEFAULT_INPUT += '$(lz) $(vol) $(pxx) $(pyy) $(pzz) $(press)" append '
DEFAULT_INPUT += (
    '{{hash}}.thermo.out screen no title "# step time temp etotal pe ke '
)
DEFAULT_INPUT += 'enthalpy density lx ly lz vol pxx pyy pzz press"\n'
DEFAULT_INPUT += """dump	1 all atom ${dump_freq} {{hash}}.lammpstrj
dump_modify	1 sort id append yes
restart	10000 {{hash}}.restart.1 {{hash}}.restart.2
run	${nsteps} upto
write_restart	{{hash}}.restart.1
write_data	{{hash}}.datafile_final nocoeff
"""


class Lammps(HylleraasMDInterface, NewRun):
    """Lammps interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.Runner: Runner = self.compute_settings.Runner

        self.plugin = method.get('plugin')

        self.run_settings = self.compute_settings.run_settings
        self.OutputParser: Parser = OutputParser  # type: ignore

        self.method = method
        self.template, extra_template_files = self.set_template(
            method.get('template', DEFAULT_INPUT)
        )
        self.extra_template_files = extra_template_files

        self.version: str = (
            self.check_version() if method.pop('check_version', True) else None
        )

        self.arch_type = getattr(self.compute_settings, 'arch_type', 'remote')
        self.units = self.method.get('units', Units())
        if isinstance(self.units, str):
            self.units = Units(self.units)

    @run_sequentially
    def setup(self, molecule: MoleculeLike, **kwargs) -> RunSettings:
        """Set up the calculation.

        Parameters
        ----------
        molecule : MoleculeLike
            Molecule to be calculated.
        **kwargs
            Additional keyword arguments.

        Returns
        -------
        RunSettings
            RunSettings object.

        """
        run_settings = deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        program_opt = kwargs.pop('program_opt', {})

        plugin = kwargs.get('plugin', self.plugin)

        if 'template' not in kwargs.keys():
            template = self.template
            extra_template_files = self.extra_template_files
        else:
            template, extra_template_files = self.set_template(
                kwargs['template']
            )

        data_files_lammps = []

        # if run_settings.program is not None:
        #     warnings.warn('program is set in run_settings, ignoring plugin')
        #     program = run_settings.program
        # else:
        if 'program' in kwargs.keys():
            program = kwargs['program']
        else:
            program = self._set_parallel(run_settings.ntasks, plugin)

        setup_dict: Dict[str, Any] = {'program': program}

        if molecule is None:
            return replace(run_settings, **setup_dict)

        input_str = template

        unit_map = {'metal': 'metal', 'real': 'real', 'electron': 'atomic'}
        if 'units' in input_str:
            unit = input_str.split('units')[1].split()[0]
            if unit in unit_map.keys():
                self.units = Units(unit_map[unit])
            else:
                raise ValueError(f'unit {unit} not supported')
        else:
            raise ValueError('units not found in input_str')

        mol_str = self.gen_system_str_from_molecule(molecule)

        # Temporary fix for manipulating lammps input file
        def replace_line(string, keyword, content):
            """Replace line in lammps input."""
            keyword = ' '.join(keyword.split())
            lines = string.split('\n')
            for i, line in enumerate(lines):
                if keyword in line:
                    lines[i] = keyword + ' ' + content
            return '\n'.join(lines)

        # Check if a molecule
        if not isinstance(molecule, PeriodicSystem):
            # Set non-periodic boundary conditions
            input_str = replace_line(input_str, 'boundary', 's s s')

        # Set ML models from model string
        # TODO: implement additional MLPs
        if 'model' in program_opt.keys():
            model = program_opt.pop('model')
            if 'type_map' not in molecule.properties.keys():
                raise ValueError(
                    'Model used with lammps without specifying type_map'
                    ' for system. Recommend to set it manually. '
                    'Example: Molecule(\'H 0 0 0\','
                    ' properties={\'type_map\': [\'H\']}).'
                )
            model_file = File(name=model, handler=run_settings.file_handler)
            data_files_lammps.append(model_file)

            # Check if deepmd model by '.pb' as suffix
            if str(model_file.name).endswith('.pb'):
                # Check compute_settings architecure
                if self.arch_type != 'local':
                    model_name = model_file.data_path_remote
                else:
                    model_name = model_file.data_path_local
                input_str = 'plugin load ' + 'libdeepmd_lmp.so\n' + input_str
                input_str = replace_line(
                    input_str, 'pair_style', f'deepmd {model_name}'
                )

                input_str = replace_line(input_str, 'pair_coeff', '* *')

        # Use program_opt to
        options = {
            'temperature': 300.0,
            'pressure': 1.0,
            'timestep': 0.0001,
            'nsteps': 10000,
            'dump_freq': 1000,
            'fixes': '',
        }
        options.update(program_opt)
        for key, value in options.items():
            # Check whether key is defined in input_str
            str_to_be_replaced = '{{' + key + '}}'
            if str_to_be_replaced not in input_str:
                if key in program_opt.keys():
                    raise KeyError(
                        f'{key}: {program_opt[key]} cannot be'
                        + 'formatted to string.'
                    )
            else:
                input_str = input_str.replace(str_to_be_replaced, str(value))

        hash_str = unique_filename(mol_str.split('\n') + input_str.split('\n'))
        input_filename = hash_str + '.inp'
        mol_filename = hash_str + '.data'
        output_filename = hash_str + '.lammpstrj'

        # Look for "log " in input_str
        if 'log' in input_str:
            # Remove the line
            input_str = '\n'.join(
                [line for line in input_str.split('\n') if 'log' not in line]
            )

        # Add on top
        input_str = 'log    ' + '{{hash}}.log\n' + input_str

        # replace hash_str
        input_str = input_str.replace('{{hash}}', hash_str)

        # Check that are is nothing more to replace
        if '{{' in input_str:
            list_lines = []
            for line in input_str.split('\n'):
                if '{{' in line:
                    list_lines.append(line)
            raise ValueError(
                'Input string is not properly formatted, '
                + 'missing values for: '
                + ','.join(list_lines)
            )

        input_obj = TextLikeInput(input=input_str)

        files_to_write = [File(name=mol_filename, content=mol_str)]
        files_to_write.append(
            File(
                name=input_filename,
                content=input_obj.input_str,
            )
        )
        files_to_write += extra_template_files

        setup_dict.update(
            {
                'files_to_write': files_to_write,
                'output_file': output_filename,
                'args': ['-in', str(input_filename)],
                'data_files': data_files_lammps,
            }
        )

        return replace(run_settings, **setup_dict)

    @run_sequentially
    def parse(self, output: ComputeResult):
        """Parse the output."""
        output_dict = {}
        if output.output_file is not None:
            output_dict.update(self.OutputParser.parse(output.output_file))

        if output.stdout is not None:
            output_dict.update(
                self.OutputParser.parse_stdout(output.stdout)  # type: ignore
            )  # type: ignore

        if output.stderr is not None:
            warnings.warn(output.stderr)

        # if output.add_to_results is not None:
        #     output_dict.update(output.add_to_results)

        return output_dict

    def _set_parallel(
        self, ntasks: Union[int, None], plugin: Union[str, None]
    ) -> str:
        """Set program for running LAMMPS in parallel."""
        program = 'lmp'
        if ntasks is not None:
            if ntasks > 1:
                program = 'lmp_mpi'
                # launcher = ['mpirun', '-np', str(compute_settings.ntasks)]

        else:
            if plugin is not None:
                program = 'lmp_mpi'
            else:
                program = 'lmp'

        return program

    def set_template(
        self, template: Union[str, Path]
    ) -> Tuple[str, List[Path]]:
        """Set the template file for the LAMMPS simulation.

        Args:
        ----
            template (Union[str, Path]): The path to the template file or
            folder containing the template file.

        Return:
        ------
            Tuple[str, List[Path]]: A tuple containing the contents of the
            template file and a list of extra files needed for the simulation.

        Raise:
        -----
            ValueError: If no template file is found in the folder or if
            multiple template files are found.

        """
        # TODO: Use File class more actively.
        if '\n' in str(template):
            return str(template), []
        else:  # is a folder or a filename
            if isinstance(template, str):
                template = Path(template)
                extra_template_files = []

            if template.is_dir():
                tmp_files = [str(f) for f in template.glob('*')]
                template_names = [
                    'start.lmp',
                    'template.lmp',
                    'template.in',
                    'start.in',
                    'template.lammps',
                ]
                template_file = None
                n_templates = 0
                for template_name in template_names:
                    for tmp_file in tmp_files:
                        if template_name in tmp_file:
                            template_file = tmp_file
                            n_templates = +1

                if template_file is None:
                    raise ValueError(
                        f'No template file found, template folder {template} '
                        f'must contain a template file of the naming '
                        f'convention {", ".join(template_names)}.'
                    )
                if n_templates > 1:
                    raise ValueError(
                        f'Multiple template files found, template '
                        f'folder {template} must contain only one template'
                        f' file of the naming convention '
                        f'{", ".join(template_names)}.'
                    )

                extra_template_files = [
                    Path(f) for f in tmp_files if f != template_file
                ]
                try:
                    extra_template_files = [
                        File(name=f.name, content=f.read_text())
                        for f in extra_template_files
                    ]
                except (OSError, TypeError, ValueError):
                    raise ValueError(
                        f'Could not read template files {extra_template_files}'
                        f' in folder {template}. Most likely one of the '
                        'template files is not a text file.'
                    )
            else:
                template_file = str(template.resolve())

            with open(template_file, 'r') as f:
                ret_template = f.read()
            return ret_template, extra_template_files

    def check_version(self):
        """Check lammps version."""
        # running_dict = {
        #     'program':
        #     self._set_parallel(self.compute_settings.ntasks, self.plugin)
        # }
        # if self.launcher is not None:
        #     running_dict.update({'launcher': self.launcher})

        # out = self.runner.run(running_dict)
        out = self.run(None)
        # print(out)
        # version = None
        # for line in out.split('\n'):
        #     if 'LAMMPS' in line:
        #         version = line
        try:
            version = out['version']
        except KeyError:
            raise ValueError(f'could not find LAMMPS version in {out}')

        if version not in VERSIONS_SUPPORTED:
            raise NotImplementedError(
                f' LAMMPS version {version} not supported.',
                f' Please contact {self.author} ',
            )
        return version

    def author(self):
        """Get interface authors email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no, s.l.bore@kjemi.uio.no'

    def gen_system_str_from_molecule(self, system: MoleculeLike) -> str:
        """Generate system string from molecule."""
        mol_units = system.units.length[0]
        if mol_units is None:
            raise ValueError('unit not set in molecule')

        if mol_units == 'bohr':
            fac = self.units.length[1]
        elif mol_units.lower() in 'angstroms':
            fac = self.units.length[1] / Constants.bohr2angstroms
        else:
            raise ValueError(f'unknown units {mol_units} in molecule {system}')

        atoms = system.atoms
        coordinates = system.coordinates.ravel().reshape(-1, 3) * fac

        if not isinstance(system, PeriodicSystem):
            # Make a 3x3 with diagonal elements equal to 20
            box = np.diag(np.ones(3) * 50) * fac
            # make sure coordinates are in the middle of the box
            coordinates += np.diag(box) / 2.0 - np.mean(coordinates, axis=0)

        else:
            box = system.box
            box = np.array(box).ravel().reshape(-1, 3) * fac

        # Check if system has system.properties has a type_map
        if system.properties.get('type_map', None) is not None:
            type_map = system.properties['type_map']
            types = [type_map.index(atom) for atom in atoms]
        else:
            types = []
            type_map = []
            iatom = 0
            for i, atom in enumerate(atoms):
                if atom in pt.E:
                    type_map.append(atom)
                    atoms[i] = str(iatom)
                    for j in range(i + 1, len(atoms)):
                        atoms[j] = str(iatom) if atoms[j] == atom else atoms[j]
                    iatom += 1
            types = atoms

        system_str = '\n'
        system_str += f'{len(types)} atoms\n'
        system_str += f'{len(type_map)} atom types\n'

        system_str += f'0.0 {box[0, 0]} xlo xhi\n'
        system_str += f'0.0 {box[1, 1]} ylo yhi\n'
        system_str += f'0.0 {box[2, 2]} zlo zhi\n'

        if np.any(box - np.diag(np.diag(box))):
            tv = system.to_triclinic(box)
            xy = tv[1] * np.cos(tv[5] * np.pi / 180.0)
            xz = tv[2] * np.cos(tv[4] * np.pi / 180.0)
            yz = tv[1] * tv[2] * np.cos(tv[3] * np.pi / 180.0) - xy * xz
            yz /= box[1, 1]
            system_str += f'{xy} {xz} {yz} xy xz yz\n'

        system_str += '\nMasses\n\n'

        mass_for_atoms = {}
        if hasattr(system, 'masses'):
            for i, atom in enumerate(atoms):
                mass_for_atoms[atom] = system.masses[i]
        else:
            warnings.warn(
                'System does not have a masses attribute, using '
                'periodic table to set masses.'
                'You should update to latest version of hyobj.'
            )

        # Some masses might not be defined in system, we guess from
        # periodic table. Note that these masses are not used in
        # the simulation, but only for writing the data file.
        for atom in type_map:
            if atom not in mass_for_atoms.keys():
                try:
                    mass_for_atoms[atom] = pt.to_mass(atom)
                except ValueError:
                    mass_for_atoms[atom] = 1.0
        for i, atom in enumerate(type_map):
            system_str += f'{i+1} {mass_for_atoms[atom]}\n'

        system_str += '\n'
        system_str += ' Atoms # atomic\n'
        system_str += '\n'
        for i, e in enumerate(types):
            system_str += f'{i+1} {int(e)+1} '
            system_str += f'{coordinates[i][0]} '
            system_str += f'{coordinates[i][1]} '
            system_str += f'{coordinates[i][2]} \n'

        return system_str
