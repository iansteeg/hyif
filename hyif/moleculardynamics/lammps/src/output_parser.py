from pathlib import Path
from typing import Union

from hyobj import DataSet

try:
    import MDAnalysis as Mda
except ImportError:
    Mda = None
import numpy as np

from ...abc import Parser

# from qcelemental import PhysicalConstantsContext, periodictable
# constants = PhysicalConstantsContext('CODATA2018')


class OutputParser(Parser):
    """Lammps OutputParser."""

    @classmethod
    def parse(cls, filename: Union[str, Path]) -> dict:
        """Parse LAMMPS reajectory file."""
        boxlist = []
        coords = []
        if Mda is None:
            raise ImportError('MDAnalysis is not installed.')

        results = {}
        try:
            u = Mda.Universe(str(filename), format='LAMMPSDUMP')
            for ts in u.trajectory:
                boxlist.append(
                    Mda.lib.mdamath.triclinic_vectors(u.dimensions).ravel()
                )
                coords.append(u.atoms.positions.ravel())
            num_frames = len(boxlist)
            traj = {
                'coordinates': np.array(coords).reshape(num_frames, -1),
                'box': np.array(boxlist).reshape(num_frames, -1),
            }
            results.update({'trajectory': DataSet(traj), 'is_converged': True})
        except Exception as e:
            results.update({'is_converged': False, 'error': str(e)})
            return results

        # Remove extension, and look for
        # all files with same hash, except filename
        hash_stem = Path(filename).stem

        files_to_parse = [
            f
            for f in Path(filename).parent.glob(f'{hash_stem}*')
            if f != filename
        ]
        files = {}
        for f in files_to_parse:
            ext = f.name[len(hash_stem) + 1:]  # fmt: skip
            files[ext] = str(f)
        results['files'] = files
        return results

    @classmethod
    def parse_stdout(cls, stdout: Union[str, Path]) -> dict:
        """Parse LAMMPS stdout."""
        output = {}

        content = stdout.read_text() if isinstance(stdout, Path) else stdout

        for line in content.split('\n'):
            if 'LAMMPS' in line:
                output['version'] = line
            if 'OpenMP thread(s)' in line:
                output['OpenMP_threads'] = line.split()[1]
            if 'Total wall time' in line:
                output['wall_time'] = line.split()[3]
            if 'Error' in line:
                output['error'] = line.split('Error:')[1]

        return output

    @classmethod
    def parse_dir(cls, directory: Union[str, Path]) -> dict:
        """Parse LAMMPS directory."""
