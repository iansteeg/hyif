import warnings
from abc import ABC, abstractmethod
from datetime import datetime as dt
from pathlib import Path

import numpy as np


class XtbOutput(ABC):
    """XTB outpur parser class."""

    @abstractmethod
    def parse(self, str):
        """Parse prototype."""
        pass


# class XtbSlow(XtbOutput):
#     """Class for parsing output from file."""

#     def parse(self, str):
#         """Parse output from file."""
#         return str


class Xtbexternal(XtbOutput):
    """Class for parsing non-output files."""

    @classmethod
    def read_scan(cls, path):
        """Read scan from file."""
        if not path.exists():
            warnings.warn('could not find scan file {path}')
        energies = []
        coordinates = []
        lines = path.read_text().split('\n')
        n = int(lines[0].strip())
        for i, line in enumerate(lines):
            if 'energy' in line:
                energies.append(float(line.split()[1]))
                coordinates.append([])
                for j in range(i + 1, i + 1 + n):
                    coordinates[-1].append([float(x)
                                            for x in lines[j].split()[1:]])
        return {'energies': energies, 'coordinates': coordinates}

    @classmethod
    def read_final_geometry(cls, path):
        """Read final geometry from file."""
        if not path.exists():
            warnings.warn('could not find converged geometry {path}')
        xyz = path.read_text()
        lines = xyz.split('\n')
        n = int(lines[0].strip())
        atoms = []
        coordinates = []
        for j in range(2, 2 + n):
            atom, x, y, z = lines[j].split()
            atoms.append(atom)
            coordinates.append([float(x), float(y), float(z)])
        return {'xyz': xyz, 'atoms': atoms, 'coordinates': coordinates}

    @classmethod
    def read_gradient(cls, path):
        """Read gradient from file."""
        if not path.exists():
            warnings.warn('could not find gradient file {path}')
        with open(path, 'r') as grad_file:
            lines = grad_file.read().split('\n')
        gradient = []
        num_atoms = 0
        for i, line in enumerate(lines):
            if 'Number of atoms' in line:
                num_atoms = int(lines[i + 2])
            if 'The current gradient in Eh/bohr' in line:
                for j in range(i + 2, i + 2 + 3 * num_atoms):
                    gradient.append(float(lines[j]))

        return np.array(gradient).reshape(-1, 3)

    @classmethod
    def read_hessian(cls, path):
        """Read hessian from file."""
        if not path.exists():
            warnings.warn(f'could not find hessian file {path}')
        with open(path, 'r') as hessian_file:
            lines = hessian_file.read().split('\n')

        del lines[0]

        lines = [line.split() for line in lines if line]
        hessian = []
        for line in lines:
            for val in line:
                # if '-' in val:

                #     idx = [j for j, x in enumerate(val) if x == '-']
                #     if len(idx) == 1 and idx[0] == 0:
                #         hessian.append(-float(val))
                #         continue
                #     val2 = [float(x) for x in val.split('-') if x != '']
                #     if idx[0] == 0:
                #         val2[0] == -val2[0]
                #     for j in range(1, len(idx)):
                #         val2[j] = -val2[j]

                #     for x in val2:
                #         hessian.append(x)

                # else:
                hessian.append(float(val))

        dim = int(np.sqrt(len(hessian)))
        return np.array(hessian).reshape(dim, dim)

    @classmethod
    def read_frequencies(cls, path):
        """Read vibrational frequencies from file."""
        if not path.exists():
            warnings.warn('could not find g98.out file {path}')
        with open(path, 'r') as file:
            lines = file.read().split('\n')

        frequencies = []
        for i, line in enumerate(lines):
            if 'Frequencies --' in line:
                freqs = lines[i].split()[2:]
                for freq in freqs:
                    frequencies.append(float(freq))

        return frequencies

    @classmethod
    def read_normal_modes(cls, path):
        """Read normal modes from file."""
        if not path.exists():
            warnings.warn('could not find g98.out file {path}')
        with open(path, 'r') as file:
            lines = file.read().split('\n')

        num_atoms = 0
        num_modes = 0
        for i, line in enumerate(lines):
            if 'basis functions' in line:
                num_atoms = int(lines[i - 2].split()[0])
            if 'Atom AN      X      Y      Z' in line:
                row = lines[i + 2].split()[2:]
                num_modes += len(row) // 3

        normal_modes = np.empty((num_modes, 3 * num_atoms))
        modes_tot = 0
        for i, line in enumerate(lines):
            if 'Atom AN      X      Y      Z' in line:
                row = lines[i + 2].split()[2:]
                num_modes_line = len(row) // 3
                for j in range(num_modes_line):
                    idx_mode = modes_tot + j
                    mode_start = 2 + 3 * j
                    mode_end = 2 + 3 * j + 3
                    for k in range(num_atoms):
                        row = lines[i + 1 + k].split()[mode_start:mode_end]

                        # row2 = []

                        # for val in row:
                        #     if '-' in val:
                        #         idx = [jj for jj, x in enumerate(val)
                        # if x == '-']
                        #         if len(idx) == 1 and idx[0] == 0:
                        #             row2.append(-float(val))
                        #             continue

                        #         val2 = [float(x) for x in val.split('-')
                        # if x != '']
                        #         if idx[0] == 0:
                        #             val2[0] == -val2[0]
                        #         for jj in range(1, len(idx)):
                        #             val2[jj] = -val2[jj]

                        #         for x in val2:
                        #             row2.append(x)

                        #     else:
                        #         row2.append(float(val))

                        # row = row2

                        row = [float(x) for x in row]
                        row = np.array(row)
                        normal_modes[idx_mode, 3 * k:3 * k + 3] = row[:]
                modes_tot += num_modes_line

        return normal_modes


class XtbFast(XtbOutput):
    """Class for parsing output from string."""

    def __init__(self, parser_type):

        # if parser = uxtbpy or parser == full
        self.parser_type = parser_type
        try:
            import uxtbpy
        except ImportError:
            self.pre_parser = self.dummy_parser
        else:
            self.pre_parser = uxtbpy.xtb_output_parser.XtbOutputParser()
        try:
            import uxtbpy
        except ImportError:
            self.pre_parser = self.dummy_parser
        else:
            self.pre_parser = uxtbpy.xtb_output_parser.XtbOutputParser()

    def dummy_parser(self, *args, **kwargs):
        """Mimick dummy parser."""
        return {}

    def parse(self, output) -> dict:
        """Parse entine output string."""
        isfile = False
        result: dict = {}

        try:
            path = Path(output)
        except (OSError, TypeError):
            # print('Path failed')
            string = output
        else:
            try:
                ex = path.exists()
            except (OSError, TypeError):
                # print('path.exists() failed')
                string = output
            else:
                if not ex:
                    raise FileNotFoundError(f'could not find file {path}')
                string = path.read_text()

        uxtbpy: dict = {}
        # import contextlib
        # import io
        # f = io.StringIO()
        # with contextlib.redirect_stdout(f):
        #     uxtbpy = self.pre_parser.parse(string)
        # uxtbpy_print = f.getvalue()
        # if uxtbpy_print:
        #     print('WARNING uxtbpy:', uxtbpy_print)
        # import contextlib
        # import io
        # f = io.StringIO()
        # with contextlib.redirect_stdout(f):
        #     uxtbpy = self.pre_parser.parse(string)
        # uxtbpy_print = f.getvalue()
        # if uxtbpy_print:
        #     print('WARNING uxtbpy:', uxtbpy_print)

        if isfile:
            gradfile = Path(output).with_suffix('.engrad')
            if gradfile.exists():
                result['gradient'] = Xtbexternal.read_gradient(gradfile)

            hessfile = Path(output).parent / '/hessian'
            if hessfile.exists():
                result['hessian'] = Xtbexternal.read_hessian(hessfile)

        lines = string.split('\n')

        for i, line in enumerate(lines):
            if '* xtb version' in line:
                result['version'] = line.split()[3]

        if self.parser_type == 'uxtbpy':
            raise NotImplementedError('uxtbpy parser not implemented')
            # result.update(uxtbpy)
            # return result

        for i, line in enumerate(lines):
            # if 'xtb version' in line:
            #     try:
            #         result['version'] = line.split()[3]
            #     except Exception:
            #         pass
            #     # line.split('*')[1].strip()

            if 'started run on' in line:
                date = line.split()[4]
                time = line.split()[6]
                time_data = ' '.join([date, time])
                format_data = '%Y/%m/%d %H:%M:%S.%f'
                date = dt.strptime(time_data, format_data)
                result['start_time'] = date

            if 'finished run on' in line:
                date = line.split()[4]
                time = line.split()[6]
                time_data = ' '.join([date, time])
                format_data = '%Y/%m/%d %H:%M:%S.%f'
                date = dt.strptime(time_data, format_data)
                result['end_time'] = date

            # if 'ID    Z sym.   atoms' in line:
            #     try:
            #         num_atoms = 0
            #         for j in range(i + 1, len(lines)):
            #             if '---' in lines[j]:
            #                 break
            #             atomlist = lines[j].split()[3:]
            #             atomlist = [x.split('-') for x in atomlist]
            #             atomlist = [item for sublist in atomlist for
            # item in sublist]
            #             atomlist = [int(item.replace(',', '')) for item in
            # atomlist]
            #             num_atoms = max(num_atoms, max(atomlist, default=0))
            #         result['num_atoms'] = num_atoms
            #         self.num_atoms = num_atoms
            #     except Exception:
            #         print(f'could not extract atoms from {atomlist}')
            #         print(string)

            if 'Cite this work as' in line:
                result['cite'] = lines[i + 3].split('DOI: ')[1]
                # ' '.join(lines[i+1:i+4])
            if 'Reference' in line:
                result['Reference'] = line.split()[1]
            if 'Hamiltonian' in line:
                if len(line.split()) == 4:
                    result['Hamiltonian'] = line.split()[2]
            if ':: total energy' in line:
                result['energy'] = float(line.split()[3])
            if 'HOMO-LUMO gap' in line:
                result['homo_lumo_gap'] = float(line.split()[3])
            if '# basis functions' in line:
                result['basis_functions'] = int(line.split()[4])
            if '# atomic orbitals' in line:
                result['atomic_orbitals'] = int(line.split()[4])
            if '# electrons' in line:
                result['electrons'] = int(line.split()[3])
            if 'convergence criteria satisfied after' in line:
                result['iterations'] = int(line.split()[5])

            if 'GEOMETRY OPTIMIZATION CONVERGED AFTER' in line:
                result['geometry_iterations'] = int(line.split()[5])

            # if 'final structure:' in line:
            #     self.num_atoms = int(lines[i + 2])
            #     result['num_atoms'] = self.num_atoms
            #     atoms = []
            #     coordinates = []
            #     for j in range(i + 4, i + 4 + self.num_atoms):
            #         split_line = lines[j].split()
            #         atoms.append(split_line[0])
            #         x, y, z = split_line[1:]
            #         coordinates.append([float(x), float(y), float(z)])

            #     result['atoms'] = atoms
            #     result['final_coordinates_angstrom'] = coordinates

            if 'omp threads' in line:
                result['omp_threads'] = line.split()[3]

            if 'Orbital Energies and Occupations' in line:
                ens: list = []
                occs: list = []
                homo: int = -2
                lumo: int = -2
                j = i + 4
                while j <= len(lines):
                    if '... ' in lines[j]:
                        j += 1
                        continue
                    if '---' in lines[j]:
                        break
                    # print(lines[j])
                    orb_line = lines[j].split()
                    num = int(orb_line[0])
                    en = float(orb_line[2])
                    if len(orb_line) > 3:
                        if len(orb_line) == 4 and '(LUMO)' in orb_line:
                            occ = 0.0
                        else:
                            occ = float(orb_line[1])
                    else:
                        occ = 0.0
                    for k in range(len(ens), num - 1):
                        occs.append(occ)
                        ens.append(1e6)
                    ens.append(en)
                    occs.append(occ)
                    if '(HOMO)' in lines[j]:
                        homo = num
                    if '(LUMO)' in lines[j]:
                        lumo = num
                    j += 1
                orbitals: dict = {}
                orbitals['energies'] = ens
                orbitals['occupations'] = occs
                orbitals['homo'] = homo
                orbitals['lumo'] = lumo
                result['orbitals'] = orbitals
            # if 'HL-Gap' in line:
            #     result['HL-Gap'] = float(line.split()[1])
            if 'Fermi-level' in line:
                result['fermi_level'] = float(line.split()[3])
            if 'covCN' in line:
                coordination_numbers = []
                for j in range(i, len(lines)):
                    if 'Mol.' in lines[j]:
                        break

                    cn = lines[j].split()
                    if len(cn) != 7:
                        continue
                    coordination_numbers.append(float(cn[3]))

                result['coordination_numbers'] = coordination_numbers

            # if 'Wiberg/Mayer (AO) data.' in line:
            #     wbo = np.zeros((self.num_atoms, self.num_atoms))
            #     for j in range(i + 6, len(lines)):
            #         if self.num_atoms <= 1:
            #             break
            #         if '---' in lines[j]:
            #             break
            #         split_line = lines[j].split()
            #         if '--' in lines[j]:
            #             idx1 = int(split_line[0]) - 1
            #             nconn = (len(split_line) - 5) // 3
            #             for k in range(nconn):
            #                 idx2 = int(split_line[5 + k*3]) - 1
            #                 val = float(split_line[5 + k*3 + 2])
            #                 wbo[idx1, idx2] = val
            #         else:
            #             nconn = (len(split_line)) // 3
            #             for k in range(nconn):
            #                 idx2 = int(split_line[k * 3]) - 1
            #                 val = float(split_line[k*3 + 2])
            #                 wbo[idx1, idx2] = val

            #     result['wiberg_bond_orders'] = wbo

            if 'molecular dipole:' in line:
                result['molecular_dipole'] = float(lines[i + 3].split()[4])
            if 'molecular quadrupole' in line:
                quad = {}
                quad['xx'] = float(lines[i + 4].split()[1])
                quad['xy'] = float(lines[i + 4].split()[2])
                quad['yy'] = float(lines[i + 4].split()[3])
                quad['xz'] = float(lines[i + 4].split()[4])
                quad['yz'] = float(lines[i + 4].split()[5])
                quad['zz'] = float(lines[i + 4].split()[6])
                result['molecular_quadrupole'] = quad
            if 'TOTAL ENERGY' in line:
                result['total_energy'] = line.split()[3]
            if 'GRADIENT NORM' in line:
                result['gradient_norm'] = line.split()[3]
            if 'wall-time' in line:
                module = lines[i - 1].strip().split(':')[0]
                wt_day = float(lines[i].split()[2])
                wt_hour = float(lines[i].split()[4])
                wt_min = float(lines[i].split()[6])
                wt_sec = float(lines[i].split()[8])
                wt = wt_sec + 60.0 * wt_min + 60.0 * 60.0 * wt_hour
                wt += 60.0 * 60.0 * 24.0 * wt_day
                ct_day = float(lines[i + 1].split()[2])
                ct_hour = float(lines[i + 1].split()[4])
                ct_min = float(lines[i + 1].split()[6])
                ct_sec = float(lines[i + 1].split()[8])
                ct = ct_sec + 60.0 * ct_min + 60.0 * 60.0 * ct_hour
                ct += 60.0 * 60.0 * 24.0 * ct_day

                key = module + '_wall_time'
                result[key] = wt
                key = module + '_cpu_time'
                result[key] = ct

        result.update(uxtbpy)

        return result
