from __future__ import annotations

import copy
import importlib
import pickle
import warnings
from dataclasses import replace
from pathlib import Path
from typing import Any, Dict, Optional, Union

import numpy as np
import packaging.version as pv
from hyobj import Units
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings, run_sequentially)

from ....manager import File, NewRun
from ....utils import unique_filename
from ...abc import HylleraasQMInterface, Parser
from ...molecule import Molecule

VERSIONS_SUPPORTED = ['0.2.6']


class HyQDParser(Parser):
    """Parser for HyQD."""

    @classmethod
    def parse(cls, output: Union[str, Path]) -> dict:
        """Parse MRChem output.

        Parameters
        ----------
        output: str or :obj:`pathlib.Path`
            output str or path to output file

        Returns
        -------
        dict
            parsed results

        """
        result: Dict[str, Any] = {
            'warnings': [],
            'errors': [],
        }
        with open(output, 'rb') as infile:
            pickled_result = pickle.load(infile)
            result.update(pickled_result)
        return result


class HyQD(HylleraasQMInterface, NewRun):
    """Hylleraas Quantum Dynamics (HyQD) interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        """Initialize the HyQD interface.

        Parameters
        ----------
        method : dict
            Method settings.
        compute_settings : ComputeSettings, optional
            Compute settings.

        """
        self.method = method
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.output_parser = HyQDParser
        self.scheme = method['method']
        self.basis = method['basis']
        if 'check_version' in method.keys():
            if method['check_version']:
                self.check_version()
        else:
            self.check_version()

        self.run_settings = self.compute_settings.run_settings
        self.Runner = self.compute_settings.Runner
        self.run_settings.program = 'python'
        self.units = self.method.get('units', Units())

    @run_sequentially
    def setup(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up a calculation.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.

        Returns
        -------
        RunSettings
            RunSettings object.

        """
        run_settings = copy.deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        if self.scheme.lower() == 'tdcis':
            return self.run_tdcis(molecule, **kwargs)
        elif self.scheme.lower() == 'tdcis_quasi_energy':
            return self.run_tdcis_quasi_energy(molecule, **kwargs)
        elif self.scheme.lower() == 'tdcis_absorption_spectrum':
            return self.run_tdcis_absorption_spectrum(molecule, **kwargs)
        else:
            raise NotImplementedError(
                f'Scheme {self.scheme} not implemented.'
                "Available methods: 'tdcis', 'tdcis_quasi_energy', "
                "'tdcis_absorption_spectrum'"
            )

    @property
    def author(self) -> str:
        """Return author's email adress."""
        return 'morten.ledum@kjemi.uio.no'

    def check_version(self) -> pv.Version:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        version: pv.Version = pv.parse(
            importlib.metadata.version('quantum_systems')
        )
        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' HyQD (quantum_systems) version {version} not supported.',
                f' Please contact {self.author} ',
            )
        return version

    @run_sequentially
    def get_energy(
        self, molecule: Molecule, options: Optional[dict] = {},
    ) -> float:
        """Get energy of a molecule with HyQD.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.
        options : dict, optional
            Options. Defaults to {}.

        Returns
        -------
        float
            Energy.

        """
        raise NotImplementedError()
        # result = self.run(molecule, options=options)
        # return result['energy']

    @run_sequentially
    def get_gradient(
        self,
        molecule: Molecule,
        options: Optional[dict] = {},
        gradient_type: Optional[str] = None,
    ) -> np.ndarray:
        """Compute gradient with HyQD.

        Parameters
        ----------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        options : dict, optional
            options
        gradient_type: str, optional
            gradient type ('analytical' or 'numerical')

        Returns
        -------
        np.ndarray
            gradient in unit Hartee/(Molecule.properties['unit'])

        """
        raise NotImplementedError()

    @run_sequentially
    def get_hessian(
        self,
        molecule,
        options: Optional[dict] = {},
        hessian_type: Optional[str] = None,
    ) -> np.ndarray:
        """Compute Hessian with HyQD.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        hessian_type: str, optional
            hessian type ('analytical' or 'numerical')

        Returns
        -------
        np.ndarray
            hessian in unit Hartee/(Molecule.properties['unit'])

        """
        raise NotImplementedError()

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        outfile = output.output_file
        result = {}
        result['stdout'] = output.stdout
        result['stderr'] = output.stderr
        if outfile is not None:
            result.update(self.output_parser.parse(outfile))
            result['output_file'] = str(outfile)

            if result['warnings']:
                warnings.warn('\n'.join(result['warnings']))
            if result['errors']:
                raise RuntimeError('\n'.join(result['errors']))

        return result

    def molecule2xyz_string(self, molecule: Molecule) -> str:
        """Convert Molecule object to xyz string.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.

        Returns
        -------
        str
            xyz string.

        """
        string = ''
        for atom, coord in zip(molecule.atoms, molecule.coordinates):
            string += f'{atom} {coord[0]} {coord[1]} {coord[2]}\n'
        return string

    @run_sequentially
    def run_tdcis(
        self,
        molecule: Molecule,
        options: Optional[dict] = {},
        **kwargs
    ) -> ComputeResult:
        """Run a TDCIS calculation with HyQD.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.
        options : dict, optional
            Options. Defaults to {}.

        Returns
        -------
        ComputeResult
            ComputeResult object.

        """
        molecule_string = self.molecule2xyz_string(molecule)
        basis_set = self.basis
        verbose = True if self.compute_settings.debug else False
        if 'charge' in self.method:
            charge = self.method['charge']
        else:
            charge = 0
        filename = unique_filename(
            ['tdcis', molecule_string, basis_set, str(charge), str(verbose)]
        )

        code_string = f"""import tqdm
from scipy.integrate import complex_ode
import basis_set_exchange as bse
import time
import sys
import pickle
import numpy as np
from pyscf import lib

from quantum_systems import construct_pyscf_system_rhf
from quantum_systems.time_evolution_operators import DipoleFieldInteraction

from ci_singles import CIS, TDCIS

from gauss_integrator import GaussIntegrator

lib.logger.Logger(sys.stdout, 0)  # Log level: QUIET 0, ERROR 1, WARN 2

class sine_square_laser:
    def __init__(self, E0, omega, td, phase=0.0, start=0.0):
        self.F_str = E0
        self.omega = omega
        self.tprime = td
        self.phase = phase
        self.t0 = start

    def _phase(self, t):
        if callable(self.phase):
            return self.phase(t)
        else:
            return self.phase

    def __call__(self, t):
        dt = t - self.t0
        pulse = (
            (np.sin(np.pi * dt / self.tprime) ** 2)
            * np.heaviside(dt, 1.0)
            * np.heaviside(self.tprime - dt, 1.0)
            * np.sin(self.omega * dt + self._phase(dt))
            * self.F_str
        )
        return pulse


basis_set = bse.get_basis('''{basis_set}''', fmt="nwchem")

# Do Hartree-Fock and change integrals to MO-basis.
system = construct_pyscf_system_rhf(
    molecule='''{molecule_string}''',
    basis='''{basis_set}''',
    add_spin=False,
    charge={charge},
    cart=False,
    anti_symmetrize=False,
    verbose={verbose},
)

# Compute the (field-free) CIS eigenstates
cis = CIS(system).compute_eigenstates()
eps, C = cis.eps, cis.C
if {verbose}:
    print(cis.eps[0:6] + system.nuclear_repulsion_energy)
    print()
    # Compute and print transition dipole moments from the groundstate
    # to an excited state J.
    print(f"** Transition dipole moments **")
    for J in range(1, 6):
        dipole_transitions_0_to_J = (
            np.abs(cis.compute_transition_dipole_moment(0, J)) ** 2
        )
        print(
            "|<0|x|", J, ">|^2: ", dipole_transitions_0_to_J[0], ", "
            "|<0|y|", J, ">|^2: ", dipole_transitions_0_to_J[1], ", "
            "|<0|z|", J, ">|^2: ", dipole_transitions_0_to_J[2], "  "
        )
    print()

# Set maximum field strength (E0), frequency (omega), duration of
# laser (td), total simulation time (tfinal) and polarization vector.
E0 = 0.03
omega = eps[1] - eps[0]
t_cycle = 2 * np.pi / omega
td = 3 * t_cycle
time_after_pulse = 1 * t_cycle
tfinal = np.floor(td + time_after_pulse)
pulse = sine_square_laser(E0=E0, omega=omega, td=td)

polarization = np.zeros(3)
polarization_direction = 2
polarization[polarization_direction] = 1
system.set_time_evolution_operator(
    DipoleFieldInteraction(pulse, polarization_vector=polarization)
)
if {verbose}:
    print(
        "E0=", E0, ", omega=", omega, ", "
        "1 optical cycle=", t_cycle, "a.u."
    )

# Set integrator and initial state
tdcis = TDCIS(system)
integrator = complex_ode(tdcis).set_integrator(
    "GaussIntegrator", s=3, eps=1e-10
)
Ct = np.complex128(C[:, 0])
integrator.set_initial_value(Ct)


dt = 1e-2
num_steps = int(tfinal / dt) + 1
if {verbose}:
    print(f"number of time steps=", num_steps)

time_points = np.zeros(num_steps)

rho_t = tdcis.compute_one_body_density_matrix(time_points[0], Ct)

dipole_moment = np.zeros(num_steps, dtype=np.complex128)
dipole_moment[0] = tdcis.compute_one_body_expectation_value(
    rho_t, system.dipole_moment[polarization_direction]
)

populations = np.zeros((6, num_steps))
for j in range(6):
    populations[j, 0] = np.abs(np.vdot(C[:, j], integrator.y)) ** 2


for i in tqdm.tqdm(
    range(num_steps - 1),
    disable=not {verbose},
):
    time_points[i + 1] = (i + 1) * dt
    Ct = integrator.integrate(integrator.t + dt)

    rho_t = tdcis.compute_one_body_density_matrix(
        time_points[i + 1], Ct
    )

    dipole_moment[i + 1] = tdcis.compute_one_body_expectation_value(
        rho_t,
        system.dipole_moment[polarization_direction],
    )

    for j in range(6):
        populations[j, i + 1] = np.abs(np.vdot(C[:, j], Ct)) ** 2

with open('''{filename}.pickle''', 'wb') as outfile:
    pickle.dump(
        {{
            'energies': eps,
            'time_points': time_points,
            'populations': populations,
            'dipole_moment': dipole_moment,
            'dipole_transitions_0_to_J': dipole_transitions_0_to_J,
        }},
        outfile,
    )
"""
        code_file = File(name=filename + '.py', content=code_string)
        output_file = File(name=filename + '.pickle')
        files_to_write = [code_file]
        running_dict = {
            'program': 'python3',
            'args': [code_file.name],
            'output_file': output_file,
            'files_to_write': files_to_write,
        }
        return replace(self.run_settings, **running_dict)

    @run_sequentially
    def run_tdcis_quasi_energy(
        self,
        molecule: Molecule,
        options: Optional[dict] = {},
        **kwargs
    ) -> ComputeResult:
        """Run a TDCIS calculation with HyQD computing quasi energies.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.
        options : dict, optional
            Options. Defaults to {}.

        Returns
        -------
        ComputeResult
            ComputeResult object.

        """
        if 'E0' not in self.method.keys():
            warnings.warn('Setting default value for E0 = 0.001')
        E0 = self.method.get('E0', 0.001)  # noqa: N806

        if 'omega' not in self.method.keys():
            warnings.warn('Setting default value for omega = 0.2')
        omega = self.method.get('omega', 0.2)

        if 'phase' not in self.method.keys():
            warnings.warn('Setting default value for phase = 0.0')
        phase = self.method.get('phase', 0.0)

        n_cycles_ramp = self.method.get('n_cycles_ramp', 3)
        n_cycles_post_ramp = self.method.get('n_cycles_post_ramp', 1)
        pol_dir = self.method.get('pol_dir', 2)
        time_step = self.method.get('time_step', 0.1)

        molecule_string = self.molecule2xyz_string(molecule)
        basis_set = self.basis
        verbose = True if self.compute_settings.debug else False
        filename = unique_filename(
            ['tdcis_quasi_energy', molecule_string, basis_set, str(E0),
             str(omega), str(phase), str(n_cycles_ramp),
             str(n_cycles_post_ramp), str(pol_dir), str(time_step),
             str(verbose)]
        )

        code_string = f"""import numpy as np
from matplotlib import pyplot as plt
import pickle
import tqdm
import basis_set_exchange as bse

from quantum_systems import construct_pyscf_system_rhf
from quantum_systems.time_evolution_operators import DipoleFieldInteraction

from ci_singles import CIS, TDCIS


class QuadraticSwitch:
    def __init__(self, t0, t1):
        self.t0 = t0
        self.t1 = t1
        self.t_mid = 0.5*(t1 - t0)
        self.alpha = 2/(t1-3*t0)**2

    def name(self):
        return 'quadratic'

    def __call__(self, t):
        if type(t) == np.ndarray:
            # t is assumed in ascending order and uniformly spaced
            dt = t[1] - t[0]
            indx_0 = int(self.t0/dt)
            indx_m = int(self.t_mid/dt)
            indx_1 = int(self.t1/dt)
            res = np.empty(t.shape[0])
            res[:indx_0] = 0.
            res[indx_0:indx_m] = self.alpha*(t[indx_0:indx_m] - self.t0)**2
            res[indx_m:indx_1] = -self.alpha*(t[indx_m:indx_1]- self.t1)**2 + 1
            res[indx_1:] = 1.
        else:
            if t < self.t0:
                res = 0.
            elif t <= self.t_mid:
                res = self.alpha*(t - self.t0)**2
            elif t <= self.t1:
                res = -self.alpha*(t - self.t1)**2 + 1
            else:
                res = 1.
        return res


class AdiabaticLaser:
    def __init__(self, F_str, omega, phase=0.0, n_switch=1, switch='quadratic',
                 delta=None):
        self.F_str = F_str
        self.omega = omega
        self.n_switch = n_switch
        self.t_cycle = 2*np.pi/omega
        self.phase = phase
        self._envelope = self._select_envelope(switch.lower(), delta)

    def _continuous_wave(self, t):
        return self.F_str*np.cos(self.omega*t+self.phase)

    def _select_envelope(self, switch, delta):
        t0 = 0.
        t1 = self.n_switch*self.t_cycle
        if switch == 'fermi':
            if delta is None:
                eps = 1e-4*self.F_str
            else:
                eps = delta
            tau = -0.5*t1/np.log(eps/(1-eps))
            return FermiSwitch(t0, t1, tau)
        elif switch == 'linear':
            return LinearSwitch(t0, t1)
        elif switch == 'quadratic':
            return QuadraticSwitch(t0, t1)
        else:
            raise ValueError(f'Illegal switch: {{switch}}')

    def __call__(self, t):
        return self._continuous_wave(t)*self._envelope(t)

charge = 0
basis_set = bse.get_basis('''{basis_set}''', fmt="nwchem")

# Do Hartree-Fock and change integrals to MO-basis.
system = construct_pyscf_system_rhf(
    molecule='''{molecule_string}''',
    basis='''{basis_set}''',
    add_spin=False,
    charge=charge,
    cart=False,
    anti_symmetrize=False,
)

cis = CIS(system)
H0 = cis.setup_hamiltonian(system.h, system.u)

V = cis.setup_hamiltonian(
    system.dipole_moment[{pol_dir}],
    np.zeros((system.l, system.l, system.l, system.l)),
)

t_cycle = 2 * np.pi / {omega}
tfinal = t_cycle * {n_cycles_post_ramp} + {n_cycles_ramp} * t_cycle

electric_field = AdiabaticLaser(
    F_str={E0},
    omega={omega},
    phase={phase},
    n_switch={n_cycles_ramp},
    switch="quadratic",
)

I = np.complex128(np.eye(H0.shape[0]))
Ct = np.complex128(np.zeros(H0.shape[0]))
Ct[0] = 1.0 + 0j
C0 = Ct.copy()

num_steps = int(tfinal / {time_step}) + 1
if {verbose}:
    print(f"number of time steps={{num_steps}}")

time_points = np.zeros(num_steps)
dipole_moment = np.zeros(num_steps, dtype=np.complex128)
dipole_moment[0] = np.dot(Ct.conj(), V @ Ct)

Q_t = np.zeros(num_steps)
phi0_psit = np.vdot(C0, Ct)
H_psit = np.dot(H0, Ct)
phi0_H_psit = np.vdot(C0, H_psit)
Q_t[0] = (phi0_psit.conj() * phi0_H_psit).real

for i in tqdm.tqdm(
    range(num_steps - 1),
    disable=not {verbose},
):
    time_points[i + 1] = (i + 1) * {time_step}
    H_t_mid = H0 - V * electric_field(time_points[i] + {time_step} / 2)
    A_p = I + 1j * {time_step} / 2 * H_t_mid
    A_m = I - 1j * {time_step} / 2 * H_t_mid
    Ct = np.linalg.solve(A_p, A_m @ Ct)
    dipole_moment[i + 1] = np.dot(Ct.conj(), V @ Ct)

    phi0_psit = np.vdot(C0, Ct)

    H_t = H0 - V * electric_field(time_points[i + 1])
    H_psit = np.dot(H_t, Ct)
    phi0_H_psit = np.vdot(C0, H_psit)
    Q_t[i + 1] = (phi0_H_psit / phi0_psit).real

with open('''{filename}.pickle''', 'wb') as outfile:
    pickle.dump(
        {{
            'time_points': time_points,
            'electric_field': electric_field(time_points),
            'dipole_moment': dipole_moment,
            'Q_t': Q_t,
        }},
        outfile,
    )
"""
        code_file = File(name=filename + '.py', content=code_string)
        output_file = File(name=filename + '.pickle')
        files_to_write = [code_file]
        running_dict = {
            'program': 'python3',
            'args': [code_file.name],
            'output_file': output_file,
            'files_to_write': files_to_write,
        }
        return replace(self.run_settings, **running_dict)

    @run_sequentially
    def run_tdcis_absorption_spectrum(
        self,
        molecule: Molecule,
        options: Optional[dict] = {},
        **kwargs
    ) -> ComputeResult:
        """Run a TDCIS calculation with HyQD computing absorption spectra.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.
        options : dict, optional
            Options. Defaults to {}.

        Returns
        -------
        ComputeResult
            ComputeResult object.

        """
        if 'E0' not in self.method.keys():
            warnings.warn('Setting default value for E0 = 0.001')
        E0 = self.method.get('E0', 0.001)  # noqa: N806

        if 'time_step' not in self.method.keys():
            warnings.warn('Setting default value for time_step = 2e-2')
        time_step = self.method.get('time_step', 2e-2)

        if 't_final' not in self.method.keys():
            warnings.warn('Setting default value for t_final = 2000')
        t_final = self.method.get('t_final', 2000)

        molecule_string = self.molecule2xyz_string(molecule)
        basis_set = self.basis
        verbose = True if self.compute_settings.debug else False
        filename = unique_filename(
            ['tdcis_quasi_energy', molecule_string, basis_set, str(E0),
             str(t_final), str(time_step), str(verbose)]
        )

        code_string = f"""import numpy as np
import pickle
import tqdm
import basis_set_exchange as bse

from quantum_systems import construct_pyscf_system_rhf
from ci_singles import CIS

from scipy.sparse.linalg import expm_multiply, expm

charge = 0
dt = {time_step}
basis_set = bse.get_basis('''{basis_set}''', fmt="nwchem")

# Do Hartree-Fock and change integrals to MO-basis.
system = construct_pyscf_system_rhf(
    molecule='''{molecule_string}''',
    basis='''{basis_set}''',
    add_spin=False,
    charge=charge,
    cart=False,
    anti_symmetrize=False,
)

# Compute Hamiltonian and dipole moment operators
cis = CIS(system)
H0 = cis.setup_hamiltonian(system.h, system.u)
V = [
    cis.setup_hamiltonian(
        system.dipole_moment[alpha],
        np.zeros((system.l, system.l, system.l, system.l)),
    )
    for alpha in range(3)
]

if {verbose}:
    print(f"CIS Hilbert space is {{H0.shape[0]}}-dimensional.")

# Get the ground state, which in CIS is just the HF state.
C_ground_state = np.zeros(H0.shape[0], dtype=complex)
C_ground_state[0] = 1.0

# Set up time grid and laser pulse
n_steps = int({t_final} / dt)
time_points = np.linspace(0, {t_final}, n_steps + 1)
dt = time_points[1] - time_points[0]

dipole_moment = np.zeros((n_steps + 1, 3), dtype=np.float64)

# Set up exact propagator for fast post-pulse propagation
U0 = expm(-1j * dt * H0)

# Loop over polarization directions
for alpha in range(3):

    # Start in ground state
    Ct = np.complex128(C_ground_state)

    # Compute dipole moment along polarization direction for t=0,
    # just before pulse
    dipole_moment[0, alpha] = (Ct.conj().T @ (V[alpha] @ Ct)).real

    # Set up kicked wavefunction
    # by propagating through the pulse
    Ct = expm_multiply(-1j * {E0} * V[alpha], Ct)

    # Do time integration after pulse
    for i in tqdm.tqdm(
        range(n_steps),
        disable=not {verbose},
    ):
        # Exact solution of TDSE
        Ct = U0 @ Ct

        # Compute dipole moment along polarization direction for
        # t=time_points[i+1]
        dipole_moment[i + 1, alpha] = (Ct.conj().T @ (V[alpha] @ Ct)).real

with open('''{filename}.pickle''', 'wb') as outfile:
    pickle.dump(
        {{
            'time_points': time_points,
            'dipole_moment': dipole_moment,
            'n_steps': n_steps,
            'dt': dt,
        }},
        outfile,
    )
"""
        code_file = File(name=filename + '.py', content=code_string)
        output_file = File(name=filename + '.pickle')
        files_to_write = [code_file]
        running_dict = {
            'program': 'python3',
            'args': [code_file.name],
            'output_file': output_file,
            'files_to_write': files_to_write,
        }
        return replace(self.run_settings, **running_dict)
