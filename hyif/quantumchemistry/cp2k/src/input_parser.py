from pathlib import Path
from typing import Any, Dict, List, Union

import numpy as np
from hyobj import Constants

from ...abc import Parser


class InputParser(Parser):
    """CP2K Input Parser."""

    @classmethod
    def parse(cls, inpath) -> Dict[str, Any]:
        """Parse input."""
        result: Dict[str, Any] = {}
        try:
            path = Path(inpath)
        except (OSError, TypeError):
            lines = inpath.split('\n')
        else:
            try:
                ex = path.exists()
            except OSError:
                ex = False
            if not ex:
                lines = inpath.split('\n')
            else:
                with open(path, 'rt') as f:
                    lines = f.readlines()

        result['input_file'] = str(inpath)

        for i, line in enumerate(lines):
            if '&CELL' in line:
                raw_cell = []
                order = []
                for j in range(i + 1, i + 4):
                    line_split = lines[j].split()
                    order.append(line_split[0])
                    # Check if [bohr] is present
                    if line_split[1].lower() == '[bohr]':
                        vals = [float(line_split[k]) for k in range(2, 5)]
                    else:
                        vals = [float(line_split[k]) / Constants.bohr2angstroms
                                for k in range(1, 4)]
                    raw_cell.append(np.array(vals))

                result['box'] = np.array(
                    [x for _, x in sorted(zip(order, raw_cell))])
            if 'PERIODIC NONE' in line:
                result.pop('box', [40, 0, 0, 0, 40, 0, 0, 0, 40])
            if '&COORD' in line:
                xyz: List[Union[List[float], float]] = []
                atoms: List[str] = []
                for j in range(i + 1, len(lines)):
                    if '&END COORD' in lines[j]:
                        break
                    if 'UNIT' in lines[j]:
                        unit = lines[j].split()[1].lower()
                        fac = 1.0
                        if unit == 'angstrom':
                            fac /= Constants.bohr2angstroms
                        # if lines[j].split()[1].lower() != 'angstrom':
                        #     raise ValueError('Only angstroms supported')
                        continue

                    line_split = lines[j].split()
                    atoms.append(line_split[0])
                    vals = [float(line_split[k]) for k in range(1, 4)]
                    xyz.append(np.array(vals))

                result['atoms'] = atoms
                result['coordinates'] = np.array(xyz,
                                                 dtype=np.float64).ravel()*fac

        return result
