import numpy as np
import packaging.version as pv
# import pytest
# import pytest
from hyobj import Constants, Molecule, PeriodicSystem, Units
from hyset import create_compute_settings

from hyif import CP2K  # type: ignore

mol_str = """H 0.0 0.0 0.0
 H 0.0 0.0 0.74"""
mymol1 = PeriodicSystem(Molecule(mol_str, units='angstrom'),
                        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]])

mol2 = f'H 0.0 0.0 0.0\n H 0.0 0.0 {0.74/Constants.bohr2angstroms}'
pbc = 5.0/Constants.bohr2angstroms*np.eye(3)
mymol2 = PeriodicSystem(Molecule(mol2, units='bohr'), pbc=pbc)

ddir = '/Users/tilmann/Documents/work/hylleraas/hyif/'
ddir += 'hyif/quantumchemistry/cp2k'
myenv_cp2k = create_compute_settings('local',
                                     smp_threads=8,
                                     force_recompute=False,
                                     debug=False,
                                     env_vars={'CP2K_DATA_DIR': ddir})
# mycp2k = CP2K({}, compute_settings = myenv_cp2k)


def test_version_check():
    """Test cp2k version check."""
    mycp2k = CP2K({'check_version': False}, myenv_cp2k)
    version = mycp2k.check_version()
    assert version >= pv.Version('7.1.0')


def test_setup():
    """Test cp2k setup."""
    mycp2k = CP2K({}, compute_settings=myenv_cp2k)
    setup = mycp2k.setup(mymol1)
    assert 'PROJECT cp2k' in setup.files_to_write[0][1]
    # print(setup.files_to_write[0][1])
    # with pytest.raises(ValueError):
    #     mycp2k.InputParser.parse(setup.files_to_write[0][1])
    # np.testing.assert_allclose(parsed['coordinates'],
    #                            mymol1.coordinates.ravel()*1.8897259886,
    #                            rtol=1e-6, atol=1e-6)


def test_run():
    """Test cp2k run."""
    mycp2k = CP2K({}, compute_settings=myenv_cp2k)
    result = mycp2k.run(mymol1)
    ref_en = -1.161696966614423
    np.testing.assert_almost_equal(result['energy'],
                                   ref_en, decimal=4)
    en = mycp2k.get_energy(mymol1)
    np.testing.assert_almost_equal(en,
                                   ref_en, decimal=4)

    en2 = mycp2k.get_energy(mymol2)
    np.testing.assert_almost_equal(en, en2, decimal=4)

    mycp2k2 = CP2K({'units': Units('real')}, compute_settings=myenv_cp2k)
    en3 = mycp2k2.get_energy(mymol1)

    np.testing.assert_almost_equal(en3, 627.509469*ref_en, decimal=4)

    en4 = mycp2k.get_energy(mymol1, units=Units('metal'))
    np.testing.assert_almost_equal(en4, 27.211386245988*ref_en, decimal=4)

    grad = mycp2k.get_gradient(mymol1)
    grad_ref = np.array([[-0., -0., 0.015942],
                         [-0., -0., -0.015942]])
    np.testing.assert_allclose(grad, grad_ref, rtol=1e-5, atol=1e-5)


# def test_units():
#     """Test cp2k units."""
#     grad1 = mycp2k.get_gradient(mymol1)
#     mycp2k2 = CP2K({'units': Units('real')})
#     grad2 = mycp2k2.get_gradient(mymol1)
#     # fac = constants.hartree2kcalmol/constants.bohr2angstrom
#     fac = 627.509469/0.52917721092
#     np.testing.assert_allclose(grad1*fac,
#                                grad2, rtol=1e-5, atol=1e-5)


def test_parallel():
    """Test parallel input."""
    myenv = create_compute_settings('local', mpi_procs=2, smp_threads=4)
    mycp2k = CP2K({}, compute_settings=myenv)
    setup = mycp2k.setup(None)
    assert setup.launcher[2] == '2'
    assert setup.program == 'cp2k.psmp'

    myenv = create_compute_settings('local', smp_threads=6)
    mycp2k = CP2K({}, compute_settings=myenv)
    setup = mycp2k.setup(None)
    assert setup.smp_threads == 6
    assert setup.program == 'cp2k.ssmp'

    # myenv = create_compute_settings('local')
    # mycp2k = CP2K({}, compute_settings=myenv)
    # setup = mycp2k.setup(None)
    # assert setup.program == 'cp2k.sopt'
