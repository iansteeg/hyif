import copy
from dataclasses import replace
from functools import wraps
from typing import Callable


def presetup(func: Callable):
    """Pre-set up decorator."""
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        """Wrap function."""
        rs = getattr(self, 'run_settings',
                     getattr(self.compute_settings, 'run_settings'), None)
        if rs is None:
            raise ValueError('No run_settings found')
        run_settings = copy.deepcopy(rs)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        if args is None:
            return run_settings

        return func(self, *args, run_settings=run_settings, **kwargs)
    return wrapper
