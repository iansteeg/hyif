from dataclasses import dataclass

import numpy as np


class Units:
    """Interface to hyobj Units class."""

    length: str = None


@dataclass
class Molecule:
    """Interface to hylleraas Molecule class."""

    atoms: list = None
    coordinates: np.array = None
    properties: dict = None
    units: Units = None


@dataclass
class MoleculeLike:
    """Interface to hylleraas MoleculeLike base class."""

    atoms: list
    coordinates: np.array
    properties: dict
