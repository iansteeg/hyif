
class OutputParser:
    """Parser for crest output."""

    @classmethod
    def parse(cls, output: str) -> dict:
        """Parse crest output."""
        output_dict: dict = {}
        lines = output.split('\n')
        for i, line in enumerate(lines):
            if 'Version' in line:
                version = line.split()[1].split(',')[0]
                output_dict.update({'version': version})
            if 'WARNING' in line:
                warnings = line
                j = i
                while lines[j] != '':
                    j += 1
                    warnings += lines[j]
                output_dict.update({'warnings': warnings})
            if 'Overall wall time' in line:
                output_dict.update({'wall_time':
                                    line.split(':')[1:]})
            if 'Final Geometry Optimization' in line:
                j = i
                while '-------------' not in lines[j]:
                    j += 1
                    if 'E lowest' in lines[j]:
                        output_dict.update({'energy':
                                            float(lines[j].split()[3])})
                    if 'T /K' in lines[j]:
                        output_dict.update({'temperature':
                                            float(lines[j].split()[3])})
                    if 'ensemble average energy' in lines[j]:
                        output_dict.update({'ensemble_average_energy':
                                            float(lines[j].split()[5])})
                    if 'ensemble entropy' in lines[j]:
                        output_dict.update({'ensemble_entropy':
                                            float(lines[j].split()[9])/1000})
                    if 'population of lowest in %' in lines[j]:
                        output_dict.update({'population':
                                            float(lines[j].split()[5])})
                output_dict.update({'geometry': lines[i:j]})
        return output_dict
