from pathlib import Path
from typing import List, Union

import numpy as np
from hyobj import Constants

from ....utils import read_from_str
from ...abc import Parser

ERROR_EXCEPTIONS: List[str] = ['DIIS Error', '||Error||_2']
WARNING_EXCEPTIONS: List[str] = ['Please study these warnings very carefully']
bohr2angstrom = Constants.bohr2angstroms


class OrcaOutput(Parser):
    """Orca output class."""

    @classmethod
    def parse(cls, output: Union[str, Path]) -> dict:
        """Parse orca output.

        Parameter
        ---------
        output: str or :obj:`pathlib.Path`
            output str or path to output file

        Returns
        -------
        dict
            parsed results

        """
        lines = read_from_str(output)

        result: dict = {}
        errors: list = []
        warnings: list = []

        result['is_converged'] = False

        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if 'Program Version' in line:
                result['version'] = line.split()[2]

            if 'FINAL SINGLE POINT ENERGY' in line:
                result['energy'] = float(line.split()[4])

            if 'parallel MPI-processes' in line:
                result['mpi_procs'] = int(line.split()[4])

            if 'DIPOLE MOMENT' in line:
                OrcaOutput._extract_dipole_moment(lines, result)

            if 'TOTAL RUN TIME:' in line:
                parts = line.split()
                days = float(parts[3])
                hours = float(parts[5])
                minutes = float(parts[7])
                seconds = float(parts[9])
                msec = float(parts[11])
                result['walltime'] = (
                    days * 24 * 3600
                    + hours * 3600
                    + minutes * 60
                    + seconds
                    + msec / 1000
                )
            if 'ORCA TERMINATED NORMALLY' in line:
                result['is_converged'] = True

            if 'Error' in line:
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

            if 'ERROR' in line:
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

            if 'warning' in line.lower():
                warning = ' '.join((line, lines.__next__()))
                if not any([ex in warning for ex in WARNING_EXCEPTIONS]):
                    warnings.append(warning)

        try:
            output = Path(output)
            fn_grad = output.parent / (output.stem + '.engrad')
            gradient = cls.extract_gradient(fn_grad)
            result['forces'] = (-np.array(gradient)).flatten().tolist()
        except (FileNotFoundError, IndexError):
            pass

        result['errors'] = errors
        result['warnings'] = warnings

        # try:
        #     output_path = Path(output)
        # except (OSError, TypeError):
        #     pass
        # else:
        #     if Path(output_path.stem + '.hess').exists():
        #         result['hessian'] = OrcaOutput.extract_hessian(output_path)
        #         result['frequencies'] = (
        #                                 OrcaOutput.
        #                                 extract_vibrational_frequencies(
        #                                                 output_path)
        #                                 )
        #         result['normal_modes'] = (
        #                                  OrcaOutput.
        #                                  extract_normal_modes(output_path)
        #                                  )

        #     if Path(output_path.stem + '.engrad').exists():
        #         result['gradient'] = OrcaOutput.extract_gradient(output_path)

        #     if Path(output_path.stem + '.xyz').exists():
        #         result['geometry'] = OrcaOutput.extract_geometry(output_path)

        #         if geo_conv:
        #             result['final_geometry_bohr'] = result['geometry']
        #             result['final_geometry_angstrom'] = (result['geometry']
        #                                                  * bohr2angstrom)

        return result

    # @classmethod
    # def energy(cls, output):
    #     """Extract energy from output file."""
    #     with open(output, 'r') as outfile:
    #         lines = outfile.readlines()

    #     energies = []
    #     for i, line in enumerate(lines):
    #         if 'FINAL SINGLE POINT ENERGY' in line:
    #             energies.append(float(line.split()[4]))
    #     if len(energies) > 0:
    #         energy = energies[-1]
    #     else:
    #         warnings.warn(f'could not find energy in file {output}')
    #         energy = None
    #     return energy

    @classmethod
    def extract_gradient(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract gradient from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            gradient

        """
        gradient: list = []
        lines = list(read_from_str(filename))
        n_atoms = int(lines[3])
        # energy = float(lines[7])
        for i in range(11, 11 + 3 * n_atoms):
            gradient.append(float(lines[i]))

        return np.array(gradient).ravel().reshape(-1, 3)

    @classmethod
    def extract_geometry(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract geometry from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            geometry (in bohr)

        """
        xyz: list = []
        lines = read_from_str(filename)
        lines.__next__()  # n_atoms
        lines.__next__()  # comment line

        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            xyz.append([float(x) for x in line.split()[1:4]])

        xyz = np.array(xyz) / bohr2angstrom

        return xyz

    @classmethod
    def _extract_dipole_moment(cls, lines, result):
        """Extract the dipole moment data from consecutive output block.

        This routine extracts the dipole moment vector and its magnitude in
        atomic units, and stores these in the result dictionary.

        Parameters
        ----------
        lines : iterator
            An iterator over the lines of the Orca output file
        result : dict
            The result dictionary to which the extracted dipole moment data
            will be added.

        Returns
        -------
        None
            This method directly modifies the 'result' dictionary and does
            not return any value

        """
        while True:
            try:
                line = next(lines)
            except StopIteration:
                break  # End of file

            if 'Total Dipole Moment' in line:
                parts = line.split()
                x, y, z = float(parts[4]), float(parts[5]), float(parts[6])
                result['dipole_vector'] = [x, y, z]

            elif 'Magnitude (a.u.)' in line:
                result['dipole_moment'] = float(line.split()[3])
                break

    @classmethod
    def extract_hessian(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract hessian from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            hessian

        """
        lines = read_from_str(filename)
        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if '$hessian' in line:
                ndim = int(next(lines))
                break

        hessian = np.empty((ndim, ndim))

        for row in range(ndim // 5 + 1):
            if row == ndim // 5 and ndim % 5 == 0:
                break
            ii = [int(x) for x in next(lines).split()]
            for i in range(ndim):
                ls = next(lines).split()
                jj = int(ls[0])
                hessian[ii, jj] = np.array(
                    [float(x) for x in ls[1: len(ii) + 1]]  # fmt: skip
                )

        return hessian.T

    @classmethod
    def extract_vibrational_frequencies(
        cls, filename: Union[str, Path]
    ) -> np.ndarray:
        """Extract vibrational frequencies from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            vibrational frequencies im cm-1

        """
        lines = read_from_str(filename)
        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if '$vibrational_frequencies' in line:
                ndim = int(next(lines))
                break

        freqs = np.empty(ndim)
        for i in range(ndim):
            freqs[i] = float(next(lines).split()[1])

        return freqs

    @classmethod
    def extract_normal_modes(cls, filename: Union[str, Path]) -> np.ndarray:
        """Extract normal_modes from output file.

        Parameter
        ---------
        filename : str or :obj:`pathlib.Path`
            path to output file

        Returns
        -------
        np.ndarray
            normal modes

        """
        lines = read_from_str(filename)
        while True:
            line: str = ''
            try:
                line = next(lines)
            except StopIteration:
                break

            if '$normal_modes' in line:

                ndim1, ndim2 = [int(w) for w in next(lines).split()]
                break

        modes = np.empty((ndim1, ndim2))

        for row in range(ndim1 // 5 + 1):
            if row == ndim1 // 5 and ndim1 % 5 == 0:
                break
            ii = [int(x) for x in next(lines).split()]
            for i in range(ndim2):
                ls = next(lines).split()
                jj = int(ls[0])
                modes[ii, jj] = np.array(
                    [float(x) for x in ls[1: len(ii) + 1]]  # fmt: skip
                )

        return modes.T
