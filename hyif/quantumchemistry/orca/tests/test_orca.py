import os

import numpy as np
import pytest
from hyobj import Constants, Molecule
from hyset import create_compute_settings

from hyif import Orca

bin = '/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/'

myenv = create_compute_settings(
    'local',
    path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/',
    force_recompute=True, print_level=0, debug=False)

myenv4 = create_compute_settings(
    'local',
    path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/',
    force_recompute=True, mpi_procs=4, memory=1000, debug=False)


def test_school_2024():
    """Test school 2024."""
    import hylleraas as hsp
    wdir = os.path.realpath('./')+'/school_wrk'
    local_env = hsp.create_compute_settings('local',
                                            path=bin,
                                            debug=True, force_recompute=True,
                                            work_dir=wdir)
    water = hsp.Molecule('O')

    # print("Orca blyp/pcseg-1 energy of water:", orca_blyp.get_energy(water))
    orca_blyp = hsp.Method({'qcmethod': ['DFT', 'BLYP'], 'basis': 'pcseg-1'},
                           program='orca', compute_settings=local_env)
    print('Orca blyp/pcseg-1 gradient of water:',
          orca_blyp.get_gradient(water))


def test_integration_units():
    """Test unit integration."""
    mymol = Molecule('H 0 0 1 \n H 0 0 -1', units='bohr')
    val = Constants.bohr2angstroms
    mymol1 = Molecule(f'H 0 0 {val} \n H 0 0 {-val}',
                      units='angstrom')
    myorca = Orca({'qcmethod': ['uhf', 'scfconv10']}, compute_settings=myenv)
    en_bohr = myorca.run(mymol)['energy']
    en_angs = myorca.run(mymol1)['energy']

    np.testing.assert_allclose(en_bohr, en_angs, atol=1e-7)


def test_parallel():
    """Test parallel input."""
    mymol = Molecule('O')
    myorca = Orca({'qcmethod': 'BP86', 'basis': 'def2-SV(P)'},
                  compute_settings=myenv4)
    out = myorca.run(mymol)
    assert out['mpi_procs'] == 4


@pytest.fixture(scope='session', params=['water.inp'])
def get_input_file(request):
    """Fixture returning molecule filename."""
    yield os.path.join(pytest.FIXTURE_DIR, request.param)


def test_input(get_input_file):
    """Test reader in hygo.Molecule class."""
    # with pytest.raises(NotImplementedError):
    #     myorca = Orca({})
    mymol = Orca.get_input_molecule(get_input_file)
    np.testing.assert_allclose(
        mymol.coordinates,
        np.array([[0.0000, 0.0000, 0.0626], [-0.7920, 0.0000, -0.4973],
                  [0.7920, 0.0000, -0.4973]]))
    assert mymol.properties['charge'] == 0
    m, s = Orca.get_input_method(get_input_file)
    assert 'HF' in m
    assert 'SCF' in s.keys()
    myorca = Orca(
        {
            'maxcore': 10000,
            'input_file': get_input_file, 'basis': 'aug-cc-pV6Z',
            'properties': 'transition_state', 'qcmethod': ['MP2F12', 'RIMP2'],
            'main_input': ['D3'
                           ], 'specific_input': {'Method': {'AngularGrid': 4}}
        },
        compute_settings=myenv)

    assert 'HF' in myorca.main_input
    assert 'D3' in myorca.main_input
    assert 'SCF' in myorca.specific_input.keys()
    assert 'Method' in myorca.specific_input.keys()
    assert 'ri-mp2' in myorca.main_input
    assert 'MP2F12' in myorca.main_input
    assert 'aug-cc-pv6z' in myorca.main_input
    assert 'OptTS' in myorca.main_input
    assert 'uio.no' in myorca.author


def test_integration(get_input_file):
    """Test integration."""
    mymol = Molecule(Orca.get_input_molecule(get_input_file))
    myorca = Orca({'qcmethod': 'BP86', 'basis': 'def2-SV(P)'},
                  compute_settings=myenv)
    en = myorca.get_energy(mymol)
    np.testing.assert_allclose(en, -74.914092410267, atol=1e-6)
    grad = myorca.get_gradient(mymol)
    np.testing.assert_allclose(grad, [[
        -9.70000000e-11, -7.90000000e-11, -3.43556932e+00
    ], [2.49270911e+00, 4.70000000e-11, 1.71778466e+00
        ], [-2.49270911e+00, 3.20000000e-11, 1.71778466e+00]],
                               rtol=1e-6,
                               atol=1e-6)
    hessian = myorca.get_hessian(mymol)
    np.testing.assert_allclose(
        hessian.T, [[
            1.38910138e+01, -2.00408821e-12, 7.19839965e-11, -6.94727115e+00,
            -4.63383317e-10, -7.08018853e+00, -6.94727115e+00, 4.73814255e-10,
            7.08018853e+00
        ],
                    [
                        -1.96545455e-12, -6.13745256e+00, 4.83385121e-10,
                        -6.23569686e-10, 3.06783495e+00, -2.77623919e-10,
                        6.09375584e-10, 3.06783495e+00, -2.70787784e-10
                    ],
                    [
                        7.20092682e-11, 4.80470234e-10, 4.06588099e+00,
                        -6.77232957e+00, -3.46316184e-10, -2.03187823e+00,
                        6.77232957e+00, -3.00976074e-10, -2.03187823e+00
                    ],
                    [
                        -6.94550433e+00, -3.46814638e-10, -6.77307808e+00,
                        7.03528636e+00, 6.45218757e-10, 6.92626211e+00,
                        -8.80178015e-02, 1.23755919e-10, -1.53911602e-01
                    ],
                    [
                        -1.91284447e-10, 3.06873198e+00, -2.90184173e-10,
                        6.43758920e-10, -3.10744765e+00, 3.58328912e-10,
                        -1.65378678e-10, 3.96069919e-02, -1.59269700e-11
                    ],
                    [
                        -7.08037006e+00, -2.23837763e-10, -2.03295365e+00,
                        6.92622130e+00, 3.59368347e-10, 1.94482575e+00,
                        1.53913541e-01, -5.01774332e-11, 8.70656276e-02
                    ],
                    [
                        -6.94550433e+00, 3.50078907e-10, 6.77307808e+00,
                        -8.80178015e-02, -1.82323243e-10, 1.53911602e-01,
                        7.03528636e+00, -5.97972974e-10, -6.92626211e+00
                    ],
                    [
                        2.19005497e-10, 3.06873198e+00, -2.84351991e-10,
                        1.07348196e-10, 3.96069919e-02, -5.36559446e-11,
                        -5.96907901e-10, -3.10744765e+00, 3.49164187e-10
                    ],
                    [
                        7.08037006e+00, -2.55681673e-10, -2.03295365e+00,
                        -1.53913541e-01, -1.43778371e-11, 8.70656276e-02,
                        -6.92622130e+00, 3.49873221e-10, 1.94482575e+00
                    ]],
        rtol=1e-4,
        atol=1e-4)
    output = myorca.get_normal_modes(mymol)
    np.testing.assert_allclose(
        output['normal_modes'],
        [[
            0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
            0.00000000e+00, 0.00000000e+00, 1.37185342e-13, 8.39428339e-13,
            7.25644428e-02
        ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, -2.75839064e-17, 1.60200645e-17,
             5.40821709e-18
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, -7.06732002e-02, -4.99013971e-02,
             7.34347998e-13
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, -4.27708946e-01, 5.84743195e-01,
             -5.75872282e-01
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, 7.72326978e-17, 4.07890803e-17,
             -9.62921844e-17
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, 5.60863358e-01, 3.96018081e-01,
             -4.07109710e-01
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, 4.27708946e-01, -5.84743195e-01,
             -5.75872282e-01
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, -2.75719935e-17, -6.03661919e-17,
             2.57156652e-17
         ],
         [
             0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
             0.00000000e+00, 0.00000000e+00, 5.60863358e-01, 3.96018081e-01,
             4.07109710e-01
         ]],
        rtol=1e-4,
        atol=1e-4)

    myorca_geo = Orca(
        {
            'qcmethod': 'BP86', 'basis': 'def2-SV(P)',
            'properties': ['geometry_optimization']
        },
        compute_settings=myenv)
    geo = myorca_geo.run(mymol)
    np.testing.assert_allclose(geo['final_geometry_angstrom'], [[
        -0.000000, 0.000000, 0.240324
    ], [-0.770864, 0.000000, -0.366758], [0.770864, 0.000000, -0.366758]],
                               rtol=1e-6,
                               atol=1e-6)

    myorca_cgeo = Orca(
        {
            'qcmethod': 'BP86', 'basis': 'def2-SV(P)', 'constrain': [
                'angle', 1, 0, 2, 107
            ], 'properties': ['geometry_optimization']
        },
        compute_settings=myenv)
    cgeo = myorca_cgeo.run(mymol)
    xyz = cgeo['final_geometry_bohr']
    v1 = xyz[1, :] - xyz[0, :]
    v1 = v1 / np.linalg.norm(v1)
    v2 = xyz[2, :] - xyz[0, :]
    v2 = v2 / np.linalg.norm(v2)

    angle = np.arccos(v1 @ v2) * 180.0 / np.pi
    np.testing.assert_allclose(angle, 107, rtol=1e-6, atol=1e-6)
